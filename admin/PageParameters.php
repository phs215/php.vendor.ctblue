<?php
namespace CTBlue\web\Model;

use CTBlue\web\DAO\Databases\Database;

class PageParameters
{
    private $parameters = array();
    /** @var $db Database */
    private $db = null;
    private $page_name = null;

    /**
     * PageParameters constructor.
     * @param $db Database
     */

    function __construct($db, $page)
    {
        $this->db = $db;
        $this->page_name = $page;
        $this->loadParams();
    }

    public function loadParams()
    {
        //load parameters for the current page
        $cmd = 'SELECT * FROM admin_page_parameters WHERE page_name=? AND disabled=0';
        if ($res = $this->db->returnFromDb($cmd, array($this->page_name))) {
            foreach ($res as $r) {
                $this->parameters[$r['variable']] = $r['value'];
            }
        }
    }

    public function getParameters($param, $default='')
    {
        if (isset($this->parameters[$param])) {
            return $this->parameters[$param];
        }else{
            $this->setParameter($param,$default);
            return $default;
        }
    }

    public function setParameter($variable, $value)
    {
        if(isset($this->parameters[$variable])){
            //update
            $cmd='UPDATE admin_page_parameters SET `value`=? WHERE page_name=? AND variable=?';
            $this->db->runQuery($cmd,array($value, $this->page_name, $variable));
        }else{
            //insert
            $cmd='INSERT INTO admin_page_parameters (page_name,variable,`value`) VALUES (?,?,?)';
            $this->db->query($cmd, array($this->page_name,$variable, $value));
        }
        $this->parameters[$variable] = $value;
    }

    /**
     * @param $db Database
     */
    public static function install($db)
    {
        $db->query("CREATE TABLE IF NOT EXISTS `admin_page_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(1000) NOT NULL,
  `variable` varchar(100) NOT NULL,
  `value` varchar(1000) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
    }
}