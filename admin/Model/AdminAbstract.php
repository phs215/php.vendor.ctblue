<?php
namespace ctblue\web\admin\Model;

use ctblue\web\Utils\Http;
use DAO\DbManager;

abstract class AdminAbstract
{
    public $global_orderby = 'DESC';
    public $td = -1;
    public $id = null;
    public $unique_id = '';
    public $parent_unique_id = '';
    public $parameterTitle = '';
    public $current_result = array();
    public $table = '';
    public $parent_column = null;
    public $all = array();
    /** @var AdminAbstract $parent */
    public $parent = null;
    /** @var AdminAbstract $child */
    public $child = null;

    public abstract function  GetAll($parent_id);

    public abstract function  GetAllWithFilter($column, $value);

    public abstract function  DeleteSingle($id);

    public abstract function  GetSingle($id);

    public abstract function  Update($id);

    public abstract function  Add();

    /**
     * @param $r
     * @return bool
     */
    public abstract function loadMeArray($r);


    public function loadByFieldName($col, $value)
    {
        $cmd = 'SELECT * FROM ' . $this->table . ' WHERE ' . $col . '=?';
        $res = DbManager::$db->returnFromDb($cmd, array($value));
        $this->loadMeArray($res);
    }

    public function loadByMultipleFieldName($cols = [], $values = [])
    {
        $cmd = 'SELECT * FROM ' . $this->table . ' WHERE ';
        $c = count($cols);
        for ($i = 0; $i < $c; $i++) {
            $cmd .= "`" . $cols[$i] . "`=?";
            if ($i < $c - 1) $cmd .= ' AND ';
        }
        $res = DbManager::$db->returnFromDb($cmd, $values);
        return $this->loadMeArray($res);
    }


    public function load($unique_id, $title)
    {
        $this->unique_id = $unique_id;
        $this->parameterTitle = $title;
        //load td and id
        if ($td = Http::Get('td')) {
            $this->td = $td;
        }
        if ($td = Http::Post('td')) {
            $this->td = $td;
        }
        if (!$this->td) $this->td = 'td';
        if ($id = Http::Get('id' . $this->unique_id)) {
            $this->id = $id;
        }
        if ($this->table == '') {
            throw new \Exception('Table name is empty in AdminAbstract');
        }
    }

    /**
     * @param $parent AdminAbstract
     */
    public function SetParent($parent, $parent_column)
    {
        $this->parent = $parent;
        $this->parent_unique_id = $parent->unique_id;
        $this->parent_column = $parent_column;
        $this->parent->SetChild($this);
    }

    /**
     * Set the child tab of the current tab
     * @param $child AdminAbstract
     */
    public function SetChild($child)
    {
        $this->child = $child;
    }

    public function GetParentForURL()
    {
        if ($this->parent != null) {
            $url = '';
            //get parent
            $url .= '&id' . $this->parent->unique_id . '=' . $this->parent->id;
            $url .= $this->parent->GetParentForURL();
            return $url;
        }
        return '';
    }

    /**
     * Loads all the objects with a single sql query
     * @param mixed
     * @param $cmd
     * @param $vars
     * @return array
     */
    public static function loadAllBySQL($objectSample, $cmd, $vars = null)
    {
        $res = DbManager::$db->returnFromDb($cmd, $vars);
        $all = array();
        foreach ($res as $r) {
            $tmp = get_class($objectSample);
            $a = new $tmp;
            $a->loadMeArray($r);
            $all[] = $a;
        }
        return $all;
    }

    public function loadBySql($cmd, $vars = null)
    {
        $res = DbManager::$db->returnFromDb($cmd, $vars);
        if (isset($res[0])) {
            $this->loadMeArray($res[0]);
            return true;
        }
        return false;
    }

    public function GetCurrentIdForURL()
    {
        return '&id' . $this->unique_id . '=' . $this->id;
    }

    public function SetSingleRow($row_id)
    {
        $this->id = $row_id;
    }

    public static function deleteById($table, $id)
    {
        DbManager::$db->query('DELETE FROM `' . $table . '` WHERE id=?', array($id));
    }

    public function GetParentId()
    {
        if ($this->parent != null) {
            return $this->parent->id;
        } else return -1;
    }

    public function Query()
    {
        if (http::Post('save')) {
            if ('save-' . $this->unique_id == $this->td) {
                if ($this->id == -1 || $this->id == null) {
                    $this->Add();
                } else {
                    $this->Update($this->id);
                }
            }
        }
        if ($this->td === 'delete-' . $this->unique_id) {
            $this->DeleteSingle($this->id);
            $this->id = false;
        } else if ($this->id) {
            $this->current_result = $this->GetSingle($this->id);
        }
//        $this->all=$this->GetAll();
    }
}