<?php
namespace ctblue\web\DAO\ObjectManagement;

use ctblue\web\DAO\Databases\Database;
use ctblue\web\Utils\Http;

/**
 * For managing an object normally, when not working with the administration
 * Class CRUDBuilderX
 * @package ctblue\web\DAO\ObjectManagement
 */
class CRUDBuilderX
{
    public $version = '';
    private $cols = array();
    private $db;
    private $basedir;
    private $fieldsPadding;

    /**
     * CRUDBuilder constructor.
     * @param $db Database
     * @param $basedir String
     */
    function __construct($db, $basedir)
    {
        $this->db = $db;
        $this->basedir = $basedir;
    }

    /**
     * @param $table String table name
     * @return string
     */
    public function getModel($table)
    {
        ob_start();
        echo '<?php';
        $tmp = explode('_', $table);
        $fieldsPadding = '';
        foreach ($tmp as $t) {
            $fieldsPadding .= $t[0];
        }
        $cmd = "SHOW COLUMNS FROM `" . $table . "`";
        /** @var array $cols */
        $cols = $this->db->returnFromDb($cmd);
        //remove the modification date and creation date from the cols
        for ($i = 0; $i < count($cols); $i++) {
            if ($cols[$i]['Default'] == 'CURRENT_TIMESTAMP') {
                for ($j = $i; $j + 1 < count($cols); $j++) {
                    $cols[$j] = $cols[$j + 1];
                }
                unset($cols[count($cols) - 1]);
                $i = -1;
            }
        }
        $question_marks = '';
        $insert_columns = '';
        $update_columns = '';
        $post_lines = '';
        $c = count(explode(',', Http::get('cols')));
        ?>
        namespace model\base;
        use DAO\DbManager;
        use ctblue\web\admin\Model\AdminAbstract;
        use ctblue\web\DAO\Databases\Database;

        class <?php $class_name = str_replace(' ', '', ucwords(str_replace('_', ' ', $table)));
        echo $class_name;
        ?> extends AdminAbstract
        {
        //all the columns
        <?php
//        var_dump($cols);
        foreach ($cols as $c) {
            echo 'public $' . $c['Field'] . '=\'' . $c['Default'] . '\';' . "\r\n";
        }
        ?>
        public $original_table='<?php echo $table ?>';
        function __construct($unique_id=-1,$title='')
        {
        $this->table=$this->original_table;
        if($unique_id!=-1 && $title!=''){
        $this->load($unique_id,$title);
        }
        }
        public function loadByFieldName($col, $value){
        $cmd='SELECT * FROM <?php echo $table ?> WHERE '.$col.'=?';
        $res= DbManager::$db->returnFromDb($cmd,array($value));
        $this->loadMeArray($res);
        }
        public function deleteByFieldName($col, $value){
        $cmd='DELETE FROM <?php echo $table ?> WHERE '.$col.'=?';
        $res= DbManager::$db->query($cmd,array($value));
        }
        public function loadById($id){
        $cmd='SELECT * FROM <?php echo $table ?> WHERE id=?';
        $res= DbManager::$db->returnFromDb($cmd,array($id));
        return $this->loadMeArray($res);
        }
        public function loadMeArray($res)
        {
        if(isset($res[0]['id']))$res=$res[0];
        if(!isset($res['id']))return false;
        <?php
        foreach ($cols as $c) {
            echo '$this->' . $c['Field'] . '=$res[\'' . $c['Field'] . '\'];' . "\r\n";
        }
        ?>
        return true;
        }
        public function GetAll($parent_id=null,$enabled_only=false){
        $cmd = 'SELECT * FROM `' . $this->table.'`';
        $vars = array();
        if ($this->parent != null) {
        if ($this->parent->id != null) {
        $cmd .= ' WHERE ' . $this->parent_column . '=?';
        $vars[] = $this->parent->id;
        }
        }
        $cmd .= ' ORDER BY id DeSC';
        $res= DbManager::$db->returnFromDb($cmd,$vars);
        $all=array();
        foreach($res as $r){
        if($enabled_only)if($r['disabled']==1)continue;
        $a=new <?php echo $class_name ?>;
        $a->loadMeArray($r);
        $all[]=$a;
        }
        return $all;
        }
        public function GetAllWithFilter($column,$value)
        {
        $cmd = 'SELECT * FROM `' . $this->table.'` WHERE `'.$column.'`=?';
        return DbManager::$db->returnFromDb($cmd,array($value));
        }
        public function DeleteSingle($id){
        $cmd='DELETE FROM `' . $this->table.'` WHERE id=?';
        return DbManager::$db->query($cmd,array($id));
        }
        public function GetSingle($id){
        $cmd='SELECT * FROM `' . $this->table.'` WHERE id=?';
        $res=DbManager::$db->returnFromDb($cmd,array($id));
        if(isset($res[0]['id'])){
        $this->loadMeArray($res[0]);
        return $this;
        }
        return false;
        }
        public function delete(){
        if($this->id)
        DbManager::$db->query('DELETE FROM `' . $this->table.'` WHERE id=?',array($this->id));
        }
        public function Update($id){
        $cmd=Database::generateUpdateQuery($this->table,array(<?php
        $i = 0;
        foreach ($cols as $c) {
            if ($c['Key'] == 'PRI') {
                $i++;
                continue;
            }
            echo '\'' . $c['Field'] . '\'';
            if ($i < count($cols) - 1) echo ',';
            $i++;
        }
        ?>));
        $cmd.=' WHERE id=?';
        $v=array();
        <?php foreach ($cols as $c) {
        if ($c['Key'] == 'PRI') continue;
        $type = $this->GetDataGridType($c) . $this->version;
        echo '$v[]=$this->' . $c['Field'] . ';
    ';
    } ?>
        $v[]=$id;
        if(DbManager::$db->query($cmd,$v)){
        $this->GetSingle($id);
        return true;
        }
        return false;
        }
        public function Add(){
        $cmd=Database::generateInsertQuery($this->table,array(<?php
        $i = 0;
        foreach ($cols as $c) {
            if ($c['Key'] == 'PRI') {
                $i++;
                continue;
            }
            echo '\'' . $c['Field'] . '\'';
            if ($i < count($cols) - 1) echo ',';
            $i++;
        }
        ?>));
        $v=array();
        <?php
        foreach ($cols as $c) {
            if ($c['Key'] == 'PRI') continue;
            echo '$v[]=$this->' . $c['Field'] . ';';
        } ?>
        if(DbManager::$db->query($cmd,$v)){
        $this->GetSingle(DbManager::$db->lastInsertId);
        return true;
        }
        return false;
        }
        public function resetId(){
        $this->id='';
        }
        public function save(){
        if($this->id!=''){
        $this->updateMe();
        }else{
        //add as new;
        $this->Add();
        }
        }
        public function updateMe(){
        if($this->id=='')$this->save();
        else{
        $this->Update($this->id);
        }
        }
        }
        <?php
        $res = ob_get_clean();
        $this->cols = $cols;
        $this->fieldsPadding = $fieldsPadding;
        return $res;
    }

    /**
     * @param $db Database
     */
    public function Build($db)
    {
        $editing_form = '';
        foreach ($this->cols as $c) {
            if ($c['Key'] == 'PRI') continue;
            ob_start();
            if ($this->GetDataGridType($c) == 'index') {
                ?>
                <div class="one-line">
                    <label><?php echo ucfirst($c['Field']) ?>:</label>

                    <div class="data">
                        <?php
                        echo '<?php
                $res_' . $c['Field'] . ' = $pages[$current_page->parent_unique_id]->GetAll();
                ?>
                <select name="' . $c['Field'] . $this->fieldsPadding . '" id="' . $c['Field'] . $this->fieldsPadding . '" class="selectpicker"
                        data-style="btn-primary">
                    <?php
                    $c = count($res_' . $c['Field'] . ');
					$tmp_id=DataGridFields::GetData($current_result, \'' . $c['Field'] . '\');
                    if (!$tmp_id) {//choose from parent
                        $tmp_id=$parent_id;
                    }
                    for ($i = 0; $i < $c; $i++) {
                        $selected = \'\';
                        if ($res_' . $c['Field'] . '[$i][\'id\'] == $tmp_id) $selected = \'selected="selected"\';
                        echo \'<option value="\' . $res_' . $c['Field'] . '[$i][\'id\'] . \'" \' . $selected . \'>\' . $res_' . $c['Field'] . '[$i][\'text\'] . \'</option>\';
                    }
                    ?>'; ?>

                        </select>
                    </div>
                </div>
                <?php
            } else {
                if ($this->GetDataGridType($c) != 'fulltext') echo '<div class="one-line">';
                else echo '<div style="width:100%;clear:both">';
                ?>

                <label><?php echo ucfirst($c['Field']) ?></label>

                <div
                    class="data">
                    <?php
                    $type = $this->GetDataGridType($c);
                    $more_args = ',\'\',array()';
                    if ($type === 'fulltext') {
                        $more_args = ',\'\',array(\'width\'=>\'100%\')';
                    }
                    if ($c['Field'] === 'position') {
                        $more_args = ',0,array()';
                    }
                    echo '<?php echo DataGridFields::GetEditField($current_result, \'' . $c['Field'] . '\', \'' . $type . '\', false, false' . $more_args . ',\'' . $c['Field'] . $this->fieldsPadding . '\') ?>' ?>
                </div>
                <?php
                echo '</div>';
                ?>
                <?php
            }
            $editing_form .= ob_get_clean();
        }
        echo '<br /><br />' . $editing_form;

    }

    function GetDataGridType($column_data)
    {
        $database_type = strtolower($column_data['Type']);
        $fieldname = strtolower($column_data['Field']);
        $key = strtolower($column_data['Key']);
        if ($key == 'mul' && $database_type == 'int(11)') {
            return 'index';
        }
        if (strpos($database_type, 'varchar') !== false && (strpos($fieldname, 'image') !== false || strpos($fieldname, 'thumbnail') !== false || strpos($fieldname, 'thumb') !== false)) {
            return 'image';
        }
        if (strpos($database_type, 'tinyint(1)') !== false) {
            return 'bool';
        }
        if (strpos($database_type, 'int') !== false ||
            strpos($database_type, 'float') !== false ||
            strpos($database_type, 'decimal') !== false ||
            strpos($database_type, 'double') !== false ||
            strpos($database_type, 'real') !== false
        ) {
            return 'int';
        }
        if (strpos($database_type, 'date') !== false) {
            return 'date';
        }
        if (strpos($database_type, 'varchar') !== false) {
            if (strpos($fieldname, 'date') !== false && strpos($database_type, '(10)') !== false) {
                return 'date';
            }
            if (strpos($fieldname, 'time') !== false && strpos($database_type, '(10)') !== false) {
                return 'time';
            }
            return 'text';
        }
        if (strpos($database_type, 'longtext') !== false) {
            return 'fulltext';
        }
        if (strpos($database_type, 'mediumtext') !== false) {
            return 'textarea';
        }
        if (strpos($database_type, 'text') !== false) {
            return 'textarea';
        }
        return 'text';
    }
}

?>