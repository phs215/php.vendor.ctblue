<?php
namespace ctblue\web\DAO\ObjectManagement;

use ctblue\web\DAO\Databases\Database;

class DatabaseMapper
{
    private $basedir;
    private $db;

    /**
     * DatabaseMapper constructor.
     * @param $basedir
     * @param $db Database
     */
    function __construct($basedir, $db)
    {
        $this->db = $db;
        $this->basedir = $basedir;
    }

    public function mapModels()
    {
        $tableNames = $this->db->getTableNames();
        $crud = new CRUDBuilderX($this->db, $this->basedir);
        if (file_exists($this->basedir . '/model/base')) {
            $this->removeDirectory($this->basedir . '/model/base');
        }
        mkdir($this->basedir . '/model/base');
        foreach ($tableNames as $table) {
//            if($table!='user')continue;
            $tableName=str_replace(' ','',ucwords(str_replace('_', ' ',$table)));
            $filename = $this->basedir . '/model/base/' . $tableName . '.php';
            file_put_contents($filename, $crud->getModel($table));
//            echo $crud->getModel($table);
//            break;
        }
    }

    private function removeDirectory($path)
    {
        $files = glob($path . '/*');
        foreach ($files as $file) {
            is_dir($file) ? $this->removeDirectory($file) : unlink($file);
        }
        rmdir($path);
        return;
    }
}