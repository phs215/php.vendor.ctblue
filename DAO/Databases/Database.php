<?php
namespace ctblue\web\DAO\Databases;

use ctblue\web\Utils\DebugTools\DbCommands;
use PDO;

class Database
{
    public $con;
    public $error = "";
    public $lastInsertId = -1;

    public function returnFromDb($cmd, $a = null)
    {
        return self::getFromDb($cmd, $a);
    }

    public function Close()
    {
        $this->con = null;
    }

    public function optimize()
    {
        $cmd = 'SHOW TABLES';
        $res = $this->returnFromDb($cmd);
        foreach ($res as $r) {
            $tmp = array_values($r);
            $this->query("OPTIMIZE TABLE `" . $tmp[0] . "`");
        }
    }

    public function getTableNames()
    {
        $cmd = 'SHOW TABLES';
        $tables = array();
        if ($res = $this->returnFromDb($cmd)) {
            foreach ($res as $r) {
                $tmp = array_values($r);
                $tables[] = $tmp[0];
            }
        }
        return $tables;
    }

    public function selectColumn($array, $col)
    {
        $res = array();
        for ($i = 0; $i < sizeof($array); $i++) {
            $res[] = $array[$i][$col];
        }
        return $res;
    }

    public function getFromDb($cmd, $params = null)
    {
        $id = DbCommands::setStartTime($cmd);
        $statement = $this->con->prepare($cmd);
        try {
            $statement->execute($params);
        } catch (Exception $e) {
            if (ENABLE_MYSQL_ERRORS) {
                echo 'Error at ' . $cmd . '<br />';
                var_dump($params);
                var_dump(debug_backtrace());
                var_dump($e->getMessage());
            }
            // exit();
            return false;
        }
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        $statement = null;
        DbCommands::setEndTime($cmd, $id);
        return $result;
    }


    public function queryUnsecure($cmd)
    {
        $id = DbCommands::setStartTime($cmd);
        //print $cmd;
        if (($result = $this->con->query($cmd))) {
            return true;
        } else {
            $this->error = $this->con->error;
            return false;
        }
        DbCommands::setEndTime($cmd, $id);
        return false;
    }

    public function error()
    {
        $a = $this->error;
        $this->error = "";
        return $a;
    }

    public function setUTF8()
    {
        $this->con->exec("set names utf8");
        $this->con->exec("SET CHARACTER SET utf8");
    }

    function makeValuesReferenced($arr)
    {
        $refs = array();
        foreach ($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }

    public function query($cmd, $arr = null)
    {
        $id = DbCommands::setStartTime($cmd);
        $stmt = $this->con->prepare($cmd);
        try {
            $stmt->execute($arr);
        } catch (Exception $e) {
            if (ENABLE_MYSQL_ERRORS) {
                echo 'Error at ' . $cmd . '<br />';
                var_dump($arr);
                var_dump(debug_backtrace());
                var_dump($e->getMessage());
            }
            // exit();
            return false;
        }
        $this->lastInsertId = $this->con->lastInsertId();
        DbCommands::setEndTime($cmd, $id);
        return true;
    }

    public function decode($value)
    {
        return html_entity_decode(htmlspecialchars_decode($value));
    }

    public function encode($value)
    {
        return Database::$con->real_escape_string($value);
    }

    public function escape($value)
    {
        return self::encode($value);
    }

    public function executeQuery($cmd, $v)
    {
        return self::query($cmd, $v);
    }

    public function execute($cmd, $vars)
    {
        self::executeQuery($cmd, $vars);
    }

    public function runQuery($cmd, $v)
    {
        self::executeQuery($cmd, $v);
    }

    public function GetColumns($table)
    {
        $cmd = "SHOW COLUMNS FROM `$table`";
        $id = DbCommands::setStartTime($cmd);
        $res = Database::returnFromDb($cmd);
        $out = array();
        $c = count($res);
        for ($i = 0; $i < $c; $i++) {
            $out[] = $res[$i]['Field'];
        }
        DbCommands::setEndTime($cmd, $id);
        return $out;
    }

    public function GetColumnsAndRemove($table, $removed)
    {
        $cmd = "SHOW COLUMNS FROM `$table`";
        $id = DbCommands::setStartTime($cmd);
        $res = Database::returnFromDb($cmd);
        $out = array();
        $c = count($res);
        for ($i = 0; $i < $c; $i++) {
            $c = $res[$i]['Field'];
            if (!in_array($c, $removed)) {
                $out[] = $res[$i]['Field'];
            }
        }
        DbCommands::setEndTime($cmd, $id);
        return $out;
    }

    public static function generateInsertQuery($table_name, $args)
    {
        $cmd = 'INSERT INTO `' . $table_name . '` (';
        $c = count($args);
        $question_marks = '';
        for ($i = 0; $i < $c; $i++) {
            $cmd .= '`' . $args[$i] . '`';
            $question_marks .= '?';
            if ($i < $c - 1) {
                $cmd .= ',';
                $question_marks .= ',';
            }
        }
        return $cmd . ') VALUES (' . $question_marks . ')';
    }

    public static function generateUpdateQuery($table_name, $columns)
    {
        $cmd = 'UPDATE `' . $table_name . '` SET ';
        $c = count($columns);
        for ($i = 0; $i < $c; $i++) {
            $cmd .= '`' . $columns[$i] . '`=?';
            if ($i < $c - 1) {
                $cmd .= ',';
            }
        }
        return $cmd;
    }

    public function GetLastInsertId()
    {
        return $this->con->lastInsertId();
    }

    /**
     * will create the views by filtering with the columns and their values, can be used for administration security for example
     * @param $columns array()
     * @param $values array()
     */
    public function createFilteredView($columns, $values)
    {

    }
}

class Db extends Database
{

}

?>