<?php
namespace ctblue\web\DAO\Databases;

class DbConnectMySqli {
    public $mysqli;
    public function __construct($host, $db_name, $username, $password) {
         $this->mysqli= new mysqli($host, $username, $password, $db_name);
    }
    public function closeCon(){
        $this->mysqli->close();
    }
	public function Close(){
		$this->mysqli->close();
	}
}

?>