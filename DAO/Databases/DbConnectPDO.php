<?php
namespace ctblue\web\DAO\Databases;
use PDO;

class DbConnectPDO {
    public $pdo;
    public function __construct($host, $db_name, $username, $password) {
        $this->pdo = new PDO('mysql:host='.$host.';dbname='.$db_name, $username, $password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }
    public function closeCon(){
//        $this->pdo->close();
        $this->pdo=null;
    }
    public function Close(){
        $this->closeCon();
    }
}
//$con=new DbConnectPDO();
