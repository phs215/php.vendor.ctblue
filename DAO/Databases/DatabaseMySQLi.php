<?php
namespace ctblue\web\DAO\Databases;
class DatabaseMySQLi
{
    public $con;
    public $error = "";

    public function returnFromDb($cmd, $a = null)
    {
        return self::getFromDb($cmd, $a);
    }

    public function Close()
    {
        self::$con->close();
    }

    public function selectColumn($array, $col)
    {
        $res = array();
        for ($i = 0; $i < sizeof($array); $i++) {
            $res[] = $array[$i][$col];
        }
        return $res;
    }

    public function getFromDb($cmd, $params = null)
    {
        if ($params == null) return self::returnFromDbUnsecure($cmd);
        //print $cmd;
        $i = 0;
        if (!$a = self::$con->prepare($cmd)) {
            echo "Prepare failed: (" . self::$con->errno . ") " . self::$con->error;
        }
        $arr = array();
        if (sizeof($params) > 0 && !empty($params)) {
            $arr[0] = '';
            foreach ($params as $key => $value) {
                //print $key.' '.$value;
                if (is_array($value)) {
                    $arr[0] .= $value[0];
                    $arr[] = $value[1];
//                    $a->bind_param($value[0], $value[1]);
                } else {
                    $arr[0] .= $key;
                    $arr[] = $value;
//                    $a->bind_param($key, $value);
                }
            }
        }
        call_user_func_array(array($a, 'bind_param'), self::makeValuesReferenced($arr));
        if (!$a->execute()) {
            echo "Prepare failed: (" . self::$con->errno . ") " . self::$con->error;
            print_r($arr);
        }
        $params = array();
        $meta = $a->result_metadata();
        while ($field = $meta->fetch_field()) {
            $params[] = &$row[$field->name];
        }
        //echo $cmd;
        //print_r($params);

        call_user_func_array(array($a, 'bind_result'), self::makeValuesReferenced($params));
        $result = array();
        while ($a->fetch()) {
            foreach ($row as $key => $val) {
                $c[$key] = $val;
            }
            $result[] = $c;
        }
        $a->close();
        return $result;
    }

    public function returnFromDbUnsecure($cmd)
    {
        //print $cmd;
        $arr = "";
        $i = 0;
        if (($result = self::$con->query($cmd))) {
            if ($result->num_rows == 0)
                return false;
            while (($row = $result->fetch_array(MYSQL_ASSOC))) {
                $arr[$i] = $row;
                $i++;
            }
            $result->close();
            // self::$con->next_result();
        } else {
            self::$error = self::$con->error;
            return false;
        }
        if (!isset($arr[0]))
            return false;
        return $arr;
    }

    public function queryUnsecure($cmd)
    {
        //print $cmd;
        if (($result = self::$con->query($cmd))) {
            return true;
        } else {
            self::$error = self::$con->error;
            return false;
        }
        return false;
    }

    public function error()
    {
        $a = self::$error;
        self::$error = "";
        return $a;
    }

    public function setUTF8()
    {
        self::$con->set_charset("utf8");
    }

    function makeValuesReferenced($arr)
    {
        $refs = array();
        foreach ($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }

    public function PrepareExecuteFast($cmd, $arr)
    {
        $stm = self::$con->prepare($cmd);
        $stm->execute($arr);
    }

    public function query($cmd, $arr = null)
    {
        if ($arr == null) return self::queryUnsecure($cmd);
        $a = self::$con->prepare($cmd);
        //print_r($arr);echo $cmd;exit();
        if (sizeof($arr) > 0) {
            $types = '';
            $vars = array();
            foreach ($arr as $key => $v) {
                $types .= $v[0];
                $vars[] = $v[1];
            }
//            print_r($vars);
            call_user_func_array(array($a, "bind_param"), array_merge(array($types), self::makeValuesReferenced($vars)));
            //$res = $a->bind_param($types, $vars);
        }

        if ($a->execute()) {
            //exit();
            return true;
        } else {
            self::$error = self::$con->error;
            echo self::$error;
            exit();
            return false;
        }
    }

    public function decode($value)
    {
        return html_entity_decode(htmlspecialchars_decode($value));
    }

    public function encode($value)
    {
        return Database::$con->real_escape_string($value);
    }

    public function escape($value)
    {
        return self::encode($value);
    }

    public function executeQuery($cmd, $v)
    {
        return self::query($cmd, $v);
    }

    public function execute($cmd, $vars)
    {
        self::executeQuery($cmd, $vars);
    }

    public function runQuery($cmd, $v)
    {
        self::executeQuery($cmd, $v);
    }

    public function GetColumns($table)
    {
        $cmd = "SHOW COLUMNS FROM `$table`";
        $res = Database::returnFromDb($cmd);
        $out = array();
        for ($i = 0; $i < sizeof($res); $i++) {
            $out[] = $res[$i]['Field'];
        }
        return $out;
    }

    public function GetColumnsAndRemove($table, $removed)
    {
        $cmd = "SHOW COLUMNS FROM `$table`";
        $res = Database::returnFromDb($cmd);
        $out = array();
        for ($i = 0; $i < sizeof($res); $i++) {
            $c = $res[$i]['Field'];
            if (!in_array($c, $removed)) {
                $out[] = $res[$i]['Field'];
            }
        }
        return $out;
    }
}