<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 11/24/2019
 * Time: 9:28 PM
 */

namespace ctblue\web\api;


class ApplicationRequest
{
    public $json = null;

    function __construct()
    {
        $this->json = json_decode(file_get_contents('php://input'));
    }

    public function dumpValues()
    {
        var_dump($this->json);
    }

    public function getValue($key)
    {
        if (isset($this->json->{$key})) {
            return $this->json->{$key};
        }
    }
}