<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 11/15/2019
 * Time: 6:26 PM
 */

namespace ctblue\web\api;


use ctblue\web\Utils\ArrayUtil;
use ctblue\yii2\databases\mongodb\ActiveRecord;
use yii\helpers\Json;

class ApplicationResponse
{
    public $message = '';
    public $isError = false;
    public $token = '';
    public $data;
    public $fcmToken = '';
    public $loggedIn = false;

    function __construct($message = '', $isError = false)
    {
        $this->isError = $isError;
        $this->message = $message;
    }

    /**
     * @param $message
     * @param bool|\yii\db\ActiveRecord $activeRecord
     * @return $this
     */
    public function setError($message, $activeRecord = false)
    {
        $this->isError = true;
        $this->message = $message;
        if ($activeRecord) {
            ob_start();
            var_dump($activeRecord->errors);
            $a = ob_get_clean();
            $this->message .= '<br>' . $a;
        }
        return $this;
    }

    public function setUserType($type)
    {
        $this->userType = $type;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        $this->isError = false;
        return $this;
    }

    public function setSuccess($message = '')
    {
        $this->isError = false;
        if ($message) {
            $this->message = $message;
        }
    }

    public static function getError($message)
    {
        return new ApplicationResponse($message, true);
    }

    public function base64dataEncode()
    {
        $this->data = base64_encode(Json::encode($this->data));
    }

    public static function getMessage($message)
    {
        return new ApplicationResponse($message);
    }

    public function setTokenAsString($userToken)
    {
        $this->token = $userToken;
        if ($this->token) $this->loggedIn = true;
        return $this->token;
    }

    /**
     * @param $userToken UserTokenext
     * @return string
     */
    public function setToken($userToken)
    {
        $this->token = $userToken->token;
        if ($this->token) $this->loggedIn = true;
        return $this->token;
    }

    /**
     * @param $array
     * @return ApplicationResponse
     */
    public static function readFromArray($array)
    {
        return ArrayUtil::arrayToObject(get_class(new ApplicationResponse()), $array);
    }
}
