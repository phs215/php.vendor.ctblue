<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 1/4/2019
 * Time: 8:39 PM
 */

namespace ctblue\web\objects;


class Ctenum
{
    public $__default = '';

    public static function getAllVars()
    {
        $a = get_called_class();
        $reflection = new \ReflectionClass(get_called_class());
        $constants = $reflection->getConstants();
        $res = [];
        foreach ($constants as $key => $value) {
            if ($key == '__default') {
                continue;
            }
            $res[$key] = $value;
        }
        return $res;
    }
}