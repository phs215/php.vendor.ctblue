<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 2/2/2020
 * Time: 8:27 PM
 */

namespace ctblue\web\assets;

/**
 * Adds the assets from the js folder
 * Class AssetManager
 * @package ctblue\web\assets
 */
class AssetManager
{
    /**
     * Add asset from the .js folder
     * @param $name string filename of the asset
     * @return string
     */
    public static function addAsset($name)
    {
        $file = __DIR__ . '/js/' . $name . '.php';
//        echo $file;exit;
        if (file_exists($file)) {
            ob_start();
            include($file);
            return ob_get_clean();
        }
        return '';
    }
}