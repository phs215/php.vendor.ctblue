<?php
include(__DIR__ . '/utils/string.utils.php');
?>
<script>
    function JUIAutocopmlete(label, fieldName, defaultValue) {
        this.label = label;
        this.fieldName = fieldName;
        this.defaultValue = defaultValue;
        this.id = '';
        this.comboId = '';
        this.onChangeFunction = null;

        this.create = function (parentElement, sourceData) {
            if (!this.defaultValue || this.defaultValue == null) this.defaultValue = '';
            if (!this.label) this.label = '';
            this.id = 'input-' + StringUtils.uniqueId();
            this.comboId = 'combo-' + StringUtils.uniqueId();
            let body = `
<div class="form-group autocomplete">`;
            if (this.label && this.label !== null) {
                body += `
    <label class="control-label" for="` + this.fieldName + `">` + this.label + `</label>
                `;
            }
            body += `<div class="input-group">
            <input id="` + this.comboId + `" type="text" name="` + this.fieldName + `" placeholder="` + this.label + `"
                   class="form-control" autocomplete="off" value="` + this.defaultValue + `">
        <div class="input-group-btn">
            <a class="btn btn-default" id="btn-all-` + this.id + `" href="javascript:void(0)">
                <i class="fas fa-angle-down"></i>
            </a>
        </div>
    </div>
</div>
        `;
            $(parentElement).append($(body));
            // console.log($(body).html())

            autocomplete(document.getElementById(this.comboId), sourceData, document.getElementById('btn-all-' + this.id), this.onChangeFunction);
        }
    }

</script>
