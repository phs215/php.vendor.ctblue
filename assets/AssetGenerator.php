<?php
namespace ctblue\web\assets;

use ctblue\web\Utils\io\DirectoryUtils;
use ctblue\web\Utils\StringUtils;

class AssetGenerator
{
    private $html = '';
    private $replacements = array();
    /** @var  \SplFileInfo */
    private $fileInfo;
    /** @var \DOMDocument */
    private $dom;
    private $basedir;
    private $logFile;
    private $grunterStatic;

    function __construct($basedir)
    {
        $this->basedir = $basedir;
    }

    public function initFilesAndFolders()
    {
        DirectoryUtils::deleteDirectory($this->basedir . '/assets');
        if (!file_exists($this->basedir . '/assets')) {
            mkdir($this->basedir . '/assets');
        }
        if (file_exists($this->basedir.'/grunter.bat')) {
            //read static
            $this->readGrunterStatic();
            unlink($this->basedir.'/grunter.bat');
        }
    }

    private function readGrunterStatic()
    {
        $content = file_get_contents($this->basedir.'/grunter.bat');
        $tmp = explode('::END STATIC', $content);
        $this->grunterStatic = $tmp[0] . '::END STATIC'."\r\n";
        return $this->grunterStatic;
    }

    public function prependGrunterStatic()
    {
        $file_data = $this->grunterStatic.file_get_contents($this->basedir.'/grunter.bat');
        file_put_contents($this->basedir.'/grunter.bat', $file_data);
    }

    public function init($filePath)
    {
        $this->fileInfo = new \SplFileInfo($filePath);
        $this->logFile = $this->basedir . '/assets/' . $this->fileInfo->getFilename() . '_log';
    }

    public function generateAssets($filePath)
    {
        $this->html = file_get_contents($filePath);
        if ($this->html == '') return;
        if (file_exists($this->logFile)) {
            unlink($this->logFile);
        }
        $this->dom = new \DOMDocument();
        $this->html = StringUtils::deleteAllBetween('<?php', '?>', $this->html);
        if ($this->html == '') return;
        $this->dom->loadHTML($this->html);
        $this->generateAssetsHead($this->basedir, 'script', 'head');
        $this->generateAssetsHead($this->basedir, 'link', 'head');
        $this->generateAssetsHead($this->basedir, 'script', 'body');
        $this->generateAssetsHead($this->basedir, 'link', 'body');
    }

    private function generateAssetsHead($basedir, $type, $location)
    {
        $ext = 'js';
        if ($type == 'link') $ext = 'css';
        $exportFilename = $this->basedir . '/assets/' . $this->fileInfo->getFilename() . '_' . $location . '.' . $ext;
        $el = $this->dom->getElementsByTagName($location);
        $sources = array();
        /** @var \DOMElement $head */
        $head = $el->item(0);
        $els = $head->getElementsByTagName($type);
        $sources = array();
        $tag = 'src';
        if ($type == 'link') $tag = 'href';
        /** @var \DOMElement $script */
        foreach ($els as $script) {
            if ($script->getAttribute($tag) != '') {
                $sources[] = $script->getAttribute($tag);
            }
        }
        if (count($sources) == 0) return;
        $this->logRemovedSources($sources);
        $input = '';
        foreach ($sources as $s) {
            $input .= $basedir . '/' . $s . ' ';
        }
        //combine all javascript sources in head
        switch ($type) {
            case 'script':
                file_put_contents('grunter.bat', 'call uglifyjs --compress --output ' . $exportFilename . ' -- ' . $input . "\r\n", FILE_APPEND);
                break;
            case 'link':
                file_put_contents('grunter.bat', 'call lessc ' . $input . ' -clean-css="--s1 --advanced" ' . $exportFilename . "\r\n", FILE_APPEND);
                break;
        }
    }

    private function logRemovedSources($sources)
    {
        $output = '';
        foreach ($sources as $s) {
            $output .= $s . "\r\n";
        }
        file_put_contents($this->logFile, $output, FILE_APPEND);
    }


    public function removeTags($html, $baseurl)
    {
        if ($html == '') return '';
        //read log
        $replacedSources = array();
        if (!file_exists($this->logFile)) {
            return '';
        } else {
            $content = file_get_contents($this->logFile);
            $tmp = explode("\r\n", $content);
            foreach ($tmp as $t) {
                if (trim($t) != '') {
                    $replacedSources[] = $t;
                }
            }
        }
        //remove all the tags in the replacedSources
//        error_reporting(0);
        $dom = new \DOMDocument();
        $dom->loadHTML($html);
        foreach ($replacedSources as $source) {
            //remove the source from the html if it is the same
            $type = 'script';
            $tag = 'src';
            if (StringUtils::endsWith($source, '.css')) {
                $type = 'link';
                $tag = 'href';
            }
            /** @var \DOMNodeList $els */
            $els = $dom->getElementsByTagName($type);
            /** @var \DOMElement $el */
            foreach ($els as $el) {
                if ($el->getAttribute($tag) == $baseurl . $source) {
                    $el->parentNode->removeChild($el);
                }
            }
        }
        $html = $dom->saveHTML();
//        $this->addItems();
//        error_reporting(E_ALL);
        $this->dom = $dom;
        $this->html = $html;
        return $html;
    }

    public function addItems($baseurl, $basedir)
    {
        $this->addItems_('head', 'js', $baseurl, $basedir);
        $this->addItems_('head', 'css', $baseurl, $basedir);
        $this->addItems_('body', 'js', $baseurl, $basedir);
        $this->addItems_('body', 'css', $baseurl, $basedir);
        return $this->html;
    }

    private function addItems_($location, $type, $baseurl, $basedir)
    {
        $filename = $this->fileInfo->getFilename() . '_' . $location . '.' . $type;
        if (file_exists($basedir . '/assets/' . $filename)) {
            $location = '</' . $location . '>';
            switch ($type) {
                case 'css':
                    $this->html = str_replace($location, '<link rel="stylesheet" href="' . $baseurl . '/assets/' . $filename . '"/>' . $location, $this->html);
                    break;
                case 'js':
                    $this->html = str_replace($location, '<script type="text/javascript" src="' . $baseurl . '/assets/' . $filename . '"></script>' . $location, $this->html);
                    break;
            }
        }
    }
}