<?php

namespace ctblue\web\registration\ErrorReporting;

abstract class ErrorManager
{
    /** @var Error[] list of errors */
    protected $errorList = array();
    /**
     * ErrorManager constructor.
     * @param $childClassTitle String child class title
     */
    function __construct($childClassTitle)
    {
        $class = new \ReflectionClass($childClassTitle);
        $staticMembers = $class->getStaticProperties();
        foreach($staticMembers as $field => &$value) {
            /** @var Error $var */
            $var=&$value;
            $this->vars[$var->redirectGet]=$var;
        }
        $this->loadErrorsGet();
    }

    public function loadErrorsGet()
    {
        foreach ($_GET as $key => $value) {
            if(isset($this->vars['&'.$key.'='.$value])){
                /** @var Error $err */
                $err=$this->vars['&'.$key.'='.$value];
                $err->IS_ACTIVE=true;
                $this->errorList[]=$err;
            }
        }
    }

    /**
     * @param $error Error
     */
    public function activateError($error)
    {
        $this->errorList[] = $error;
        $error->IS_ACTIVE = true;
    }

    public function getFirstError()
    {
        if (isset($this->errorList[0])) {
            return $this->errorList[0]->redirectGet;
        }
    }

    public function isError()
    {
        if (isset($this->errorList[0])) {
            return true;
        }
        return false;
    }

    public function printErrors()
    {
        foreach ($this->errorList as $error) {
            echo $error->title . '<br>';
        }
    }

    public function removeError()
    {

    }
}