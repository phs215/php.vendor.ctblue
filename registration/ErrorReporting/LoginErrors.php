<?php
namespace ctblue\web\registration\ErrorReporting;


class LoginErrors extends ErrorManager
{
    public static $WRONG_CREDENTIALS;
    public static $EMPTY_USERNAME_OR_PASSWORD;
    public static $PAGE_HAS_EXPIRED;
    public $vars=array();

    function __construct()
    {
        self::$WRONG_CREDENTIALS = new Error('login=wrong', 'Wrong Credentials');
        self::$EMPTY_USERNAME_OR_PASSWORD = new Error('login=empty', 'Empty username or password');
        self::$PAGE_HAS_EXPIRED = new Error('login=expired', 'Page has expired');
        parent::__construct(get_class($this));
    }

    /**
     * @param $credential Error
     * @param $text String
     */
    public function overrideCredentialText($credential, $text)
    {
        $credential->title=$text;
    }

}