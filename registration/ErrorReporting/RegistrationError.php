<?php
/**
 * Created by PhpStorm.
 * User: Philippe
 * Date: 12/6/2015
 * Time: 3:39 PM
 */

namespace ctblue\web\registration\ErrorReporting;


class RegistrationErrors extends ErrorManager
{

    public static $PAGE_HAS_EXPIRED;
    public $vars=array();

    function __construct()
    {
        self::$PAGE_HAS_EXPIRED = new Error('login=expired', 'Page has expired');
        parent::__construct(get_class($this));
    }
}