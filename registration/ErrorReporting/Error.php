<?php
namespace ctblue\web\registration\ErrorReporting;

class Error
{
    /** @var bool if set to true the error exists */
    public $IS_ACTIVE = false;

    /** @var string text string stating the error's description */
    public $title = '';

    public $redirectGet='';

    function __construct($redirectGet,$title='', $IS_ACTIVE = false)
    {
        $this->redirectGet=$redirectGet;
        if($this->redirectGet!=''){
            if($this->redirectGet[0]!='&'){
                $this->redirectGet='&'.$this->redirectGet;
            }
        }
        $this->title = $title;
        $this->IS_ACTIVE = $IS_ACTIVE;
    }
}