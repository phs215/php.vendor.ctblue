<?php
namespace ctblue\web\registration;

use ctblue\web\admin\Model\AdminAbstract;
use DAO\DbManager;
use ctblue\web\DAO\Databases\Database;
use ctblue\web\plugins\DataGridView\TableHandler;

class User extends AdminAbstract
{
    //all the columns
    public $id='';
    public $username='';
    public $password='';
    public $salt='';
    public $f_name='';
    public $m_name='';
    public $l_name='';
    public $company='';
    public $email='';
    public $dob='';
    public $country_id='';
    public $currency_id='144';
    public $city='';
    public $state='';
    public $street_address_1='';
    public $street_address_2='';
    public $zip='';
    public $phone='';
    public $occupation='';
    public $interests='';
    public $rating_buyer='0-0';
    public $rating_seller='0-0';
    public $minimum_order_price='0';
    public $date_registration='';
    public $time_registration='';
    public $hash='';
    public $verified='0';
    public $resent='0';
    public $gender='';
    public $cc='0';
    public $newsletter='';
    public $data_path='';
    public $type_id='';
    public $superuser='0';
    public $disabled='0';
    public $original_table='user';
    function __construct($unique_id=-1,$title='')
    {
        $this->table=$this->original_table;
        if($unique_id!=-1 && $title!=''){
            $this->load($unique_id,$title);
        }
    }
    public function loadMeArray($res)
    {
        if(isset($res[0]))if(isset($res[0]['id']))$res=$res[0];
        if(!isset($res['id']))return false;
        $this->id=$res['id'];
        $this->username=$res['username'];
        $this->password=$res['password'];
        $this->salt=$res['salt'];
        $this->f_name=$res['f_name'];
        $this->m_name=$res['m_name'];
        $this->l_name=$res['l_name'];
        $this->company=$res['company'];
        $this->email=$res['email'];
        $this->dob=$res['dob'];
        $this->country_id=$res['country_id'];
        $this->currency_id=$res['currency_id'];
        $this->city=$res['city'];
        $this->state=$res['state'];
        $this->street_address_1=$res['street_address_1'];
        $this->street_address_2=$res['street_address_2'];
        $this->zip=$res['zip'];
        $this->phone=$res['phone'];
        $this->occupation=$res['occupation'];
        $this->interests=$res['interests'];
        $this->rating_buyer=$res['rating_buyer'];
        $this->rating_seller=$res['rating_seller'];
        $this->minimum_order_price=$res['minimum_order_price'];
        $this->date_registration=$res['date_registration'];
        $this->time_registration=$res['time_registration'];
        $this->hash=$res['hash'];
        $this->verified=$res['verified'];
        $this->resent=$res['resent'];
        $this->gender=$res['gender'];
        $this->cc=$res['cc'];
        $this->newsletter=$res['newsletter'];
        $this->data_path=$res['data_path'];
        $this->type_id=$res['type_id'];
        $this->superuser=$res['superuser'];
        $this->disabled=$res['disabled'];
    }
    public function GetAll($parent_id=null,$enabled_only=false){
        $cmd = 'SELECT * FROM `' . $this->table.'`';
        $vars = array();
        if ($this->parent != null) {
            if ($this->parent->id != null) {
                $cmd .= ' WHERE ' . $this->parent_column . '=?';
                $vars[] = $this->parent->id;
            }
        }
        $cmd .= ' ORDER BY id DeSC';
        $res= DbManager::$db->returnFromDb($cmd,$vars);
        $all=array();
        foreach($res as $r){
            if($enabled_only)if($r['disabled']==1)continue;
            $a=new User;
            $this->loadMeArray($r);
            $all[]=$a;
        }
        return $all;
    }
    public function GetAllWithFilter($column,$value)
    {
        $cmd = 'SELECT * FROM `' . $this->table.'` WHERE `'.$column.'`=?';
        return DbManager::$db->returnFromDb($cmd,array($value));
    }
    public function DeleteSingle($id){
        $cmd='DELETE FROM `' . $this->table.'` WHERE id=?';
        return DbManager::$db->query($cmd,array($id));
    }
    public function GetSingle($id){
        $cmd='SELECT * FROM `' . $this->table.'` WHERE id=?';
        $res=DbManager::$db->returnFromDb($cmd,array($id));
        if(isset($res[0]['id'])){
            $this->loadMeArray($res[0]);
            return $this;
        }
        return false;
    }
    public function Update($id){
        $cmd=Database::generateUpdateQuery($this->table,array('username','password','salt','f_name','m_name','l_name','company','email','dob','country_id','currency_id','city','state','street_address_1','street_address_2','zip','phone','occupation','interests','rating_buyer','rating_seller','minimum_order_price','date_registration','time_registration','hash','verified','resent','gender','cc','newsletter','data_path','type_id','superuser','disabled'));
        $cmd.=' WHERE id=?';
        $v=array();
        $v[]=TableHandler::returnPostValue('usernameu','text');
        $v[]=TableHandler::returnPostValue('passwordu','text');
        $v[]=TableHandler::returnPostValue('saltu','text');
        $v[]=TableHandler::returnPostValue('f_nameu','text');
        $v[]=TableHandler::returnPostValue('m_nameu','text');
        $v[]=TableHandler::returnPostValue('l_nameu','text');
        $v[]=TableHandler::returnPostValue('companyu','text');
        $v[]=TableHandler::returnPostValue('emailu','text');
        $v[]=TableHandler::returnPostValue('dobu','date');
        $v[]=TableHandler::returnPostValue('country_idu','index');
        $v[]=TableHandler::returnPostValue('currency_idu','index');
        $v[]=TableHandler::returnPostValue('cityu','text');
        $v[]=TableHandler::returnPostValue('stateu','text');
        $v[]=TableHandler::returnPostValue('street_address_1u','text');
        $v[]=TableHandler::returnPostValue('street_address_2u','text');
        $v[]=TableHandler::returnPostValue('zipu','text');
        $v[]=TableHandler::returnPostValue('phoneu','text');
        $v[]=TableHandler::returnPostValue('occupationu','textarea');
        $v[]=TableHandler::returnPostValue('interestsu','textarea');
        $v[]=TableHandler::returnPostValue('rating_buyeru','text');
        $v[]=TableHandler::returnPostValue('rating_selleru','text');
        $v[]=TableHandler::returnPostValue('minimum_order_priceu','int');
        $v[]=TableHandler::returnPostValue('date_registrationu','date');
        $v[]=TableHandler::returnPostValue('time_registrationu','text');
        $v[]=TableHandler::returnPostValue('hashu','text');
        $v[]=TableHandler::returnPostValue('verifiedu','bool');
        $v[]=TableHandler::returnPostValue('resentu','int');
        $v[]=TableHandler::returnPostValue('genderu','bool');
        $v[]=TableHandler::returnPostValue('ccu','text');
        $v[]=TableHandler::returnPostValue('newsletteru','bool');
        $v[]=TableHandler::returnPostValue('data_pathu','text');
        $v[]=TableHandler::returnPostValue('type_idu','index');
        $v[]=TableHandler::returnPostValue('superuseru','bool');
        $v[]=TableHandler::returnPostValue('disabledu','bool');
        $v[]=$id;
        if(DbManager::$db->query($cmd,$v)){
            $this->loadMe($id);
            return true;
        }
        return false;
    }
    public function Add(){
        $cmd=Database::generateInsertQuery($this->table,array('username','password','salt','f_name','m_name','l_name','company','email','dob','country_id','currency_id','city','state','street_address_1','street_address_2','zip','phone','occupation','interests','rating_buyer','rating_seller','minimum_order_price','date_registration','time_registration','hash','verified','resent','gender','cc','newsletter','data_path','type_id','superuser','disabled'));
        $v=array();
        $v[]=TableHandler::returnPostValue('usernameu','text');
        $v[]=TableHandler::returnPostValue('passwordu','text');
        $v[]=TableHandler::returnPostValue('saltu','text');
        $v[]=TableHandler::returnPostValue('f_nameu','text');
        $v[]=TableHandler::returnPostValue('m_nameu','text');
        $v[]=TableHandler::returnPostValue('l_nameu','text');
        $v[]=TableHandler::returnPostValue('companyu','text');
        $v[]=TableHandler::returnPostValue('emailu','text');
        $v[]=TableHandler::returnPostValue('dobu','date');
        $v[]=TableHandler::returnPostValue('country_idu','index');
        $v[]=TableHandler::returnPostValue('currency_idu','index');
        $v[]=TableHandler::returnPostValue('cityu','text');
        $v[]=TableHandler::returnPostValue('stateu','text');
        $v[]=TableHandler::returnPostValue('street_address_1u','text');
        $v[]=TableHandler::returnPostValue('street_address_2u','text');
        $v[]=TableHandler::returnPostValue('zipu','text');
        $v[]=TableHandler::returnPostValue('phoneu','text');
        $v[]=TableHandler::returnPostValue('occupationu','textarea');
        $v[]=TableHandler::returnPostValue('interestsu','textarea');
        $v[]=TableHandler::returnPostValue('rating_buyeru','text');
        $v[]=TableHandler::returnPostValue('rating_selleru','text');
        $v[]=TableHandler::returnPostValue('minimum_order_priceu','int');
        $v[]=TableHandler::returnPostValue('date_registrationu','date');
        $v[]=TableHandler::returnPostValue('time_registrationu','text');
        $v[]=TableHandler::returnPostValue('hashu','text');
        $v[]=TableHandler::returnPostValue('verifiedu','bool');
        $v[]=TableHandler::returnPostValue('resentu','int');
        $v[]=TableHandler::returnPostValue('genderu','bool');
        $v[]=TableHandler::returnPostValue('ccu','text');
        $v[]=TableHandler::returnPostValue('newsletteru','bool');
        $v[]=TableHandler::returnPostValue('data_pathu','text');
        $v[]=TableHandler::returnPostValue('type_idu','index');
        $v[]=TableHandler::returnPostValue('superuseru','bool');
        $v[]=TableHandler::returnPostValue('disabledu','bool');
        if(DbManager::$db->query($cmd,$v)){
            $this->loadMe(DbManager::$db->lastInsertId);
            return true;
        }
        return false;
    }
}
