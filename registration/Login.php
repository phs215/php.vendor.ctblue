<?php
namespace ctblue\web\registration;

use ctblue\web\DAO\Databases\Database;
use ctblue\web\registration\ErrorReporting\LoginErrors;
use ctblue\web\Utils\Authentification;
use ctblue\web\Utils\Http;
use Volnix\CSRF\CSRF;

class Login
{
    /** @var $user User */
    public $user = null;
    /** @var $db Database */
    private $db = null;
    /** @var  LoginErrors */
    public $errorManager;

    /**
     * Login constructor.
     * @param $db Database
     */
    function __construct($db)
    {
        $this->errorManager = new LoginErrors();
        $this->db = $db;
    }

    public function isLoggedIn()
    {
        if ($this->user == null) {
            return $this->loadFromSession();
        }
        if ($this->user->id != '') {
            return $this->loadFromSession();
        }
        return false;
    }

    public function loadFromSession()
    {
        //check if the login information exist in the session
        if (isset($_SESSION['user'])) {
            $this->user = json_decode($_SESSION['user']);
            if ($this->user->id != '') {
                return true;
            } else {
                $this->user = '';
                unset($_SESSION['user']);
            }
        }
        return false;
    }

    /**
     * @param $error_redirect_page
     * @return bool
     */
    public function loginInWithouCSRF($error_redirect_page, $success_redirect_page)
    {
        if ($this->loadFromSession()) return true;
        //load all user information
        $username_column = 'username';
        if (isset($_POST['password'])) {
            if (($username = Http::Post('username')) && ($password = Http::Post('password'))) {
                //get username and password and check check them with database
                if (Authentification::emailValidator($username)) {
                    $username_column = 'email';
                }
                //get salt
                if ($salt = $this->getSalt($username, $username_column)) {
                    $cmd = 'SELECT * FROM `user` WHERE `' . $username_column . '`=? AND password=? AND disabled=0';
                    //if they are correct, return user id
                    $p_hash = Authentification::passwordHash($password, $salt);
                    if ($res = $this->db->returnFromDb($cmd, array($username, $p_hash))) {
                        //load user
                        if ($this->user == null && isset($res[0]['id'])) {
                            $this->user = new User();
                            $this->user->GetSingle($res[0]['id']);
                            $_SESSION['user'] = json_encode($this->user);
                            //redirect without error
                            if($success_redirect_page!=''){
                                \ctblue\web\Utils\Http::Redirect($success_redirect_page);
                            }
                            return true;
                        }
                    }
                } else {
                    $this->errorManager->activateError(LoginErrors::$WRONG_CREDENTIALS);
                }
            } else {
                $this->errorManager->activateError(LoginErrors::$EMPTY_USERNAME_OR_PASSWORD);
            }
            //redirect with error
            \ctblue\web\Utils\Http::Redirect($error_redirect_page . '?' .
                $this->errorManager->getFirstError());
        }
        return false;
    }

    /**
     * @param $error_redirect_page
     * @return bool
     */
    public function loginIn($error_redirect_page, $success_redirect_page)
    {
        if ($this->loadFromSession()) return true;
        //load all user information
        $username_column = 'username';
        if (isset($_POST['password'])) {
            if (($username = Http::Post('username')) && ($password = Http::Post('password'))) {
                if (CSRF::validate($_POST)) {
                    //get username and password and check check them with database
                    if (Authentification::emailValidator($username)) {
                        $username_column = 'email';
                    }
                    //get salt
                    if ($salt = $this->getSalt($username, $username_column)) {
                        $cmd = 'SELECT * FROM `user` WHERE `' . $username_column . '`=? AND password=? AND disabled=0';
                        //if they are correct, return user id
                        $p_hash = Authentification::passwordHash($password, $salt);
                        if ($res = $this->db->returnFromDb($cmd, array($username, $p_hash))) {
                            //load user
                            if ($this->user == null && isset($res[0]['id'])) {
                                $this->user = new User();
                                $this->user->GetSingle($res[0]['id']);
                                $_SESSION['user'] = json_encode($this->user);
                                //redirect without error
                                if ($success_redirect_page != '') {
                                    \ctblue\web\Utils\Http::Redirect($success_redirect_page);
                                }
                                return true;
                            }
                        }
                    } else {
                        $this->errorManager->activateError(LoginErrors::$WRONG_CREDENTIALS);
                    }
                } else {
                    $this->errorManager->activateError(LoginErrors::$PAGE_HAS_EXPIRED);
                }
            } else {
                $this->errorManager->activateError(LoginErrors::$EMPTY_USERNAME_OR_PASSWORD);
            }
            //redirect with error
            \ctblue\web\Utils\Http::Redirect($error_redirect_page . '?' .
                $this->errorManager->getFirstError());
        }
        return false;
    }

    /**
     * returns salt from username only
     * @param $username
     * @param $username_column
     */
    private function getSalt($username, $username_column)
    {
        $cmd = 'SELECT salt FROM `user` WHERE `' . $username_column . '`=? AND disabled=0';
        if ($res = $this->db->returnFromDb($cmd, array($username))) {
            if (isset($res[0]['salt'])) return $res[0]['salt'];
        }
        return false;
    }

    public function logOut()
    {
        if (isset($_SESSION['user'])) unset($_SESSION['user']);
        $this->user = null;
    }
}