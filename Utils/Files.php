<?php
 namespace ctblue\web\Utils;

class Files {

    public static function Read($file) {
        return file_get_contents($file);
    }

    public static function Write($file, $content) {
        file_put_contents($file, $content);
    }

    public static function Create($my_file) {
        $handle = fopen($my_file, 'w') or die('Cannot open file:  ' . $my_file); //implicitly creates file
        fclose($handle);
    }
    public static function Delete($my_file){
        unlink($my_file);
    }

}

?>