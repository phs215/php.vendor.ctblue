<?php
 namespace ctblue\web\Utils;

class WidgetUtils{
	public static function ReadAdmin($loc){
		$loc=$loc.'/widgets';
		$dirs=array();
		if ($handle = opendir($loc)) {
			/* This is the correct way to loop over the directory. */
			while (false !== ($entry = readdir($handle))) {
				if($entry=="..")continue;
				if(file_exists($loc.'/'.$entry.'/admin.php')){
					$dirs[ucfirst($entry)]='widgets/'.$entry.'/admin.php';
				}
			}
			closedir($handle);
		}
		return $dirs;
	}
	public static function ReadNames($loc){
		$loc=$loc.'/widgets';
		$dirs=array();
		if ($handle = opendir($loc)) {
			/* This is the correct way to loop over the directory. */
			while (false !== ($entry = readdir($handle))) {
				if($entry=="..")continue;
				if(file_exists($loc.'/'.$entry.'/index.php')){
					$dirs[]=$entry;
				}
			}
			closedir($handle);
		}
		return $dirs;
	}
}
?>