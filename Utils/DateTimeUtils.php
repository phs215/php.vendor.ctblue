<?php

namespace ctblue\web\Utils;

use DateTime;

class DateTimeUtils
{
    public static function ToNiceDate($date = 'Y-m-d')
    {
        $dateInfo = date_parse_from_format('Y-m-d', $date);
        $unixTimestamp = mktime(
            $dateInfo['hour'], $dateInfo['minute'], $dateInfo['second'],
            $dateInfo['month'], $dateInfo['day'], $dateInfo['year']
        );
        return date('d-M-Y', $unixTimestamp);
    }

    public static function timeStampToNiceDate($timestamp)
    {
        $date = new DateTime();
        $date->setTimestamp($timestamp);
        return $date->format('d-M-Y H:i:s');
    }

    public static function SecondsToFullDate($secs)
    {
        $year = floor($secs / 31557600);
        $r = $secs % 31557600;
        $days = floor($r / 86400);
        $r = $r % 86400;
        $hours = floor($r / 3600);
        $r = $r % 3600;
        $minutes = floor($r / 60);
        $r = $r % 60;
        return $year . 'y ' . $days . 'd ' . $hours . 'h ' . $minutes . 'm ' . $r . 's';
    }

    public static function GetCurrentDate()
    {
        return date('Y-m-d');
    }

    public static function GetCurrentTime()
    {
        return date('H-i-s');
    }

    public static function getCurrentTimeStamp()
    {
        return microtime(true);
    }

    public static function FrenchToEnglish($value, $remove_trailing_zeros = false)
    {
        $v = explode('-', $value);
        if (count($v) == 3 && strlen($v[0]) != 4) {
            if ($remove_trailing_zeros) {
                if (count($v[2]) > 0) if ($v[2][0] == '0') $v[2] = $v[2][1];
                if (count($v[1]) > 0) if ($v[1][0] == '0') $v[1] = $v[1][1];
                if (count($v[0]) > 0) if ($v[0][0] == '0') $v[0] = $v[0][1];
            }
            $value = $v[2] . '-' . $v[1] . '-' . $v[0];
        }
        return $value;
    }

    public static function GetTimeDiff($t1, $d1, $t2, $d2)
    {
        $a = date_parse_from_format('Y-m-d H:i', $d1 . ' ' . $t1);
        // var_dump($a);
        $date1 = new DateTime();
        $date1->setTimestamp(mktime($a['hour'], $a['minute'], $a['second'], $a['month'], $a['day'], $a['year']));
        $a = date_parse_from_format('Y-m-d H:i', $d2 . ' ' . $t2);
        $date2 = new DateTime();
        $date2->setTimestamp(mktime($a['hour'], $a['minute'], $a['second'], $a['month'], $a['day'], $a['year']));
        $interval = $date2->diff($date1);
        return $interval->format('%H:%I');
    }

    public static function GetTimeDiffSeconds($t1, $d1, $t2, $d2)
    {
        $a = date_parse_from_format('Y-m-d H:i', $d1 . ' ' . $t1);
        // var_dump($a);
//        echo $t1.' '.$d1;
        $date1 = new DateTime();
        $date1->setTimestamp(mktime($a['hour'], $a['minute'], $a['second'], $a['month'], $a['day'], $a['year']));
        $a = date_parse_from_format('Y-m-d H:i', $d2 . ' ' . $t2);
        $date2 = new DateTime();
        $date2->setTimestamp(mktime($a['hour'], $a['minute'], $a['second'], $a['month'], $a['day'], $a['year']));
        $interval = $date2->diff($date1);
//        var_dump($interval);
        return $interval->format('%d') * 24 * 3600 + $interval->format('%s') + $interval->format('%H') * 3600 + $interval->format('%i') * 60;
//        return $interval->format('%U');
    }

    public static function ToTimeStamp($date, $format)
    {
        if ($date == '') $date = date($format);
        $d = explode('-', $date);
        $f = explode('-', $format);
        $c = count($d);
        $hours = 0;
        $minutes = 0;
        $seconds = 0;
        $month = 0;
        $year = 0;
        $day = 0;
        for ($i = 0; $i < $c; $i++) {
            switch ($f[$i]) {
                case 'H':
                    $hours = $d[$i];
                    break;
                case 'minutes':
                    $minutes = $d[$i];
                    break;
                case 's':
                    $seconds = $d[$i];
                    break;
                case 'Y';
                    $year = $d[$i];
                    break;
                case 'm':
                    $month = $d[$i];
                    break;
                case 'd':
                    $day = $d[$i];
                    break;
            }
        }
        return mktime($hours, $minutes, $seconds, $month, $day, $year);
    }

    public static function EnglishToFrench($value, $remove_trailing_zeros = false)
    {
        $v = explode('-', $value);
        if (strlen($v[0]) == 4) {
            if ($remove_trailing_zeros) {
                if ($v[2][0] == '0') $v[2] = $v[2][1];
                if ($v[1][0] == '0') $v[1] = $v[1][1];
                if ($v[0][0] == '0') $v[0] = $v[0][1];
            }
            $value = $v[2] . '-' . $v[1] . '-' . $v[0];
        }
        return $value;
    }

    /**
     * change the timezone of a datetime
     * @param $datetime DateTime
     * @param $new_time_zone
     * @param string $old_time_zone
     * @return DateTime
     */
    public static function changeTimezone($datetime, $new_time_zone, $old_time_zone = '')
    {
        if (!$new_time_zone) {
            return $datetime;
        }
        if (is_string($datetime)) {
            $datetime = new DateTime($datetime);
        }
        if (!$old_time_zone) {
            $old_time_zone = date_default_timezone_get();
        }
//        echo $datetime->format('Y-m-d H:i:s') . "\n";
        $la_time = new \DateTimeZone($new_time_zone);
        $datetime->setTimezone($la_time);
        return $datetime;
    }
    public static function monthToArabic3Letters($mon){
        $months = array(
            "jan" => "كانون الثاني",
            "feb" => "شباط",
            "mar" => "آذار",
            "apr" => "نيسان",
            "may" => "أيار",
            "jun" => "حزيران",
            "jul" => "تموز",
            "aug" => "آب",
            "sep" => "أيلول",
            "oct" => "تشرين الأول",
            "nov" => "تشرين الثاني",
            "dec" => "كانون الأول"
        );
//        echo $months[strtolower($mon)];
        if(isset($months[strtolower($mon)]))return $months[strtolower($mon)];
        return $mon;
    }
}