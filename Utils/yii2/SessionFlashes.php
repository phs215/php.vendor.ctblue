<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/16/2017
 * Time: 12:38 AM
 */

namespace ctblue\web\Utils\yii2;


class SessionFlashes
{
    public static function withDump($type, $message, $errors)
    {
        ob_start();
        var_dump($errors);
        $a = ob_get_clean();
        \Yii::$app->session->setFlash($type, $message, $a);
    }

    public static function dataUpdate($success = true)
    {
        if ($success) {
            \Yii::$app->session->setFlash('success', 'Data updated successfully');
        } else {
            \Yii::$app->session->setFlash('error', 'Data could not be updated');
        }
    }

    public static function dataDeleted($success = true)
    {
        if ($success) {
            \Yii::$app->session->setFlash('success', 'Data deleted successfully');
        } else {
            \Yii::$app->session->setFlash('error', 'Data could not be deleted');
        }
    }

    public static function dataAdded($success = true)
    {
        if ($success) {
            \Yii::$app->session->setFlash('success', 'Data added successfully');
        } else {
            \Yii::$app->session->setFlash('error', 'Data could not be added');
        }
    }
}