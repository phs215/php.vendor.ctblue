<?php
namespace ctblue\web\Utils\yii2;

class CSRF
{
    public static function getAsInput($page)
    {
        $token = \Yii::$app->request->getCsrfToken();
        \Yii::$app->session->set('csrf-' . $page, $token);
        return '<input type="hidden" name="csrf-token" value="' . $token . '" />';
    }

    public static function checkTokenPost($page)
    {
        if ($token = \Yii::$app->request->post('csrf-token')) {
            if ($token == \Yii::$app->session->get('csrf-' . $page)) {
                \Yii::$app->session->remove('csrf-' . $page);
                return true;
            }
        }
        return false;
    }

    public static function checkTokenGet($page)
    {
        if ($token = \Yii::$app->request->get('csrf-token')) {
            if ($token == \Yii::$app->session->get('csrf-' . $page)) {
                \Yii::$app->session->remove('csrf-' . $page);
                return true;
            }
        }
        return false;
    }
}