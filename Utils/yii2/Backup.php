<?php
namespace ctblue\web\Utils\yii2;



class Backup
{
    // private variables which can be access able after creating a new object of this class
    // and it is access able to objects of this class such as functions
    // Backup string
    private $str = '';

    //

    // constructor methods for classes when creating new object of this class
    function __construct()
    {
    }

    // Get tables information such as Fields, Null Values, Default Values, Keys, Extra and etc...
    function get_table_info($tblname)
    {
        // table name
        $tbl = $tblname;
        // String variable for storing table information
        $str = "";

        $str .= "-- ---------------------------------\n\n--\n--Creating table `$tbl`\n--\n\n";

        // query for creating table
        $str .= "CREATE TABLE IF NOT EXISTS $tbl (\n";
        // string query for getting fields from table
        $str_fields = "SHOW FIELDS FROM $tbl";
        // executing query
        $fields = \Yii::$app->db->createCommand($str_fields)->queryAll();
        // array for storing keys of fields
        $keys_str = array();
        // getting fields from table
        foreach ($fields as $r) {
            // adding field name and field type
            $str .= "`" . $r['Field'] . "` " . $r['Type'] . " ";
            // if Null property is not yes or Null property is no
            if ($r['Null'] != "YES") {
                // add NOT NULL
                $str .= " NOT NULL ";
            }
            // if default value is assigned or default value is not empty then add default value
            if ((isset($r['Default']) && (!empty($r['Default'])) || ($r['Default'] == "0")))
                $str .= "DEFAULT '" . $r['Default'] . "' ";
            // if Extra property is not empty
            if ($r['Extra'] != "") {
                // then change to upper case and add Extra property
                // You can remove strtoupper if you don't want to change it to upper case
                $str .= " " . strtoupper($r['Extra']);
            }
            // check for keys
            if ($r['Key'] == "PRI")
                $keys_str[] .= "PRIMARY KEY (`" . $r['Field'] . "`)";
            if ($r['Key'] == "UNI")
                $keys_str[] .= "UNIQUE KEY (`" . $r['Field'] . "`)";
            //add coma separator after each field
            $str .= ",\n";
        }
        // run loop on keys (array) variable and add to str
        for ($i = 0; $i < (count($keys_str)); $i++) {
            $str .= $keys_str[$i];
            // if it is not end of keys then add coma
            if ($i < (count($keys_str) - 1)) {
                $str .= ",";
            }
            $str .= "\n";
        }
        // end table creation query
        $str .= ")";

        // query for getting AUTO_INCREMENT number
        $auto_query = \Yii::$app->db->createCommand("SELECT COUNT(*)+1 AS `Auto` FROM $tbl")->queryAll();
        $auto = $auto_query[0]['Auto'];
        $str .= "ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=$auto ;\n\n";
        //create database
        $str .= "--\n-- Inserting data into table `$tbl`\n--\n\n";
        $this->str .= $str;
        echo $str;
    }

    // get table contents
    function get_table_content($table)
    {
        // array variable for storing fields name
        $field = array();
        // string variable for storing table content
        $str = "";
        $str_fields = "SHOW FIELDS FROM $table";
        // executing query
        $fields = \Yii::$app->db->createCommand($str_fields)->queryAll();
        //execute query
        $res = \Yii::$app->db->createCommand("SELECT * FROM $table")->queryAll();
        // get row numbers of table
        $rows = count($res);
        // inisalize a variable and assing 0 value;
        $r = 0;
        // if records found in table then get all data
        if ($rows > 0) {
            //print 'test';
            // get fields from query
            foreach ($fields as $r) {
                // add field name to field variable
                $field[] .= $r['Field'];
            }
            // insert query for table
            $str .= "INSERT INTO `$table` (";
            // append table fields into string query
            for ($i = 0; $i <= (count($field) - 1); $i++) {
                $str .= "`" . $field[$i] . "`";
                // if current is not last field then add coma
                if ($i < (count($field) - 1)) {
                    $str .= ",";
                }
            }
            // add values
            $str .= ") VALUES \n";
            // get result from query
            foreach ($res as $fetch) {
                $str .= "(";
                // run loop on fields variable, get data and add to $str
                for ($i = 0; $i <= (count($field) - 1); $i++) {
                    // add escap caracher "\" for sql injection
                    $str .= "'" . addslashes($fetch[$field[$i]]) . "'";
                    if ($i < (count($field) - 1)) {
                        $str .= ",";
                    }
                }
                $str .= ")";
                if ($r < $rows - 1) {
                    $str .= ",";
                } else {
                    $str .= ";";
                }
                $r++;
                $str .= "\n";
            }
            $str .= "\n\n";

            $this->str .= $str;
//			echo $str;
            //print $this->str;
        }
    }

    // function for getting tables from database and table information from get_table_info function
    function get_tables()
    {
        // get all tables from database
        $cmd = "SHOW TABLES FROM `" . Database::getDbName() . "`";
        $res = \Yii::$app->db->createCommand($cmd)->queryAll();
        // checking for table(s) in database if found then execute the code
        //print_r($tables);
        //exit();
        if (count($res) > 0) {
            //if table(s) found then get tables name from database
            foreach ($res as $tables_fetch) {
                $table = $tables_fetch["Tables_in_" . Database::getDbName()];
                // and then get table information
                //print 'test';
                $this->get_table_info($table);
                $this->get_table_content($table);
            }
        }
    }

    // function for executing executing backup
    function DoBackup()
    {
        // for slow connection or  for large My SQL injection
//		ini_set("max_execution_time","10000");
        $this->str .= $this->Extra() . "\n\n";
        // get tables informtaion from tables
        $this->get_tables();
        return $this->str;
        // create .sql file for backup
        //$this->create_file();
    }

    // function for creating and downloading file from we page (every thing will print from page)
    function create_file()
    {
        header("Content-disposition: filename=" . Database::getDbName() . ".sql");
        header("Content-type: application/octetstream");
        header("Pragma: no-cache");
        header("Expires: 0");
    }

    function Extra()
    {
        $str = "";
        $str .= "-- Last Updated: 02-Nov-2009\n\n\n";
        $str .= "-- Host: " . $_SERVER['HTTP_HOST'] . "\n";
        $str .= "-- PHP Version" . phpversion() . "\n";
        $str .= "-- Creation Time: " . date('Y-M-d') . " at " . date('H:i:s') . "\n\n";
        $str .= 'SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";' . "\n\n";
        return $str;
    }
}
