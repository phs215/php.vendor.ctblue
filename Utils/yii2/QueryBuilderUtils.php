<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 10/19/2017
 * Time: 12:50 PM
 */

namespace ctblue\web\Utils\yii2;


use ctblue\web\Utils\StringUtils;
use yii\db\ActiveQuery;

class QueryBuilderUtils
{
    /**
     * query to filter between 2 dates in Search
     * @param $query ActiveQuery
     * @param $value
     * @param $column
     * @return ActiveQuery
     */
    public static function doHavingDateQuery($query, $column, $value)
    {
        if ($value) {
            if (StringUtils::containsAny($value, ['>', '<', '<=', '>=', '='])) {
                $operators = ['>=', '<=', '>', '<'];
                foreach ($operators as $operator) {
                    if (StringUtils::contains($value, $operator)) {
                        $value = str_replace($operator, '', $value);
                        $value = $operator . "DATE('$value')";
                        break;
                    }
                }
                $value="DATE($column)" . $value;
                $query->having($value);
            } else {
                $query->andFilterWhere(['like', $column, $value]);
            }
        }
        return $query;
    }

    public static function doHavingQuery($query, $column, $value, $comparisonOperator='like')
    {
        if ($value) {
            if (StringUtils::containsAny($value, ['>', '<', '<=', '>='])) {
                $query->having("$column $value");
            } else {
                $query->andFilterWhere([$comparisonOperator, $column, $value]);
            }
        }
        return $query;
    }
}