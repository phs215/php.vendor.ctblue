<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/17/2017
 * Time: 5:13 PM
 */

namespace ctblue\web\Utils\yii2;


class Database
{
    /**
     * get the db name as string from yii2
     * @return false|null|string
     * @throws \yii\db\Exception
     */
    public static function getDbName()
    {
        $database = \Yii::$app->db->createCommand("SELECT DATABASE()")->queryScalar();
        return $database;
    }
}