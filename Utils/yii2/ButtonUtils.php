<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/15/2017
 * Time: 5:36 PM
 */

namespace ctblue\web\Utils\yii2;


use yii\helpers\Html;

class ButtonUtils
{
    public static function getCreateButton($text, $filterSearch = '', $filterParam = '')
    {
        $url = 'create';
        if ($value = FilterUtils::getValue($filterSearch, $filterParam)) {
            $url .= '?' . FilterUtils::generateUrl($filterSearch, $filterParam, $value);
        }
        $data = Html::a($text, [$url], ['class' => 'btn btn-success']);
        return $data;
    }

    public static function getBackToAll($text, $filterSearch = '', $filterParam = '', $value = '')
    {
        $url = 'index';
        if ($value) {
            $url .= '?' . FilterUtils::generateUrl($filterSearch, $filterParam, $value);
        } else
            if ($value = FilterUtils::getValue($filterSearch, $filterParam)) {
                $url .= '?' . FilterUtils::generateUrl($filterSearch, $filterParam, $value);
            }
        $data = Html::a($text, [$url], ['class' => 'btn btn-primary']);
        return $data;
    }
}