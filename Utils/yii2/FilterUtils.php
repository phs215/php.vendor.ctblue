<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/15/2017
 * Time: 5:38 PM
 */

namespace ctblue\web\Utils\yii2;


use yii\db\ActiveRecord;

class FilterUtils
{
    public static function getValue($filterSearch, $filterParam)
    {
        if ($f = \Yii::$app->request->get($filterSearch)) {
            if (isset($f[$filterParam])) {
                return $f[$filterParam];
            }
        }
        if (\Yii::$app->request->get($filterParam)) return \Yii::$app->request->get($filterParam);
        return false;
    }

    public static function generateUrl($filterSearch, $filterParam, $value)
    {
        return $filterSearch . urlencode("[$filterParam]") . '=' . $value;
    }

    /**
     * @param $class ActiveRecord
     * @param $attribute string
     * @param $displayField string
     * @return array
     */
    public static function generateGridViewFilter($class, $attribute, $displayField)
    {
        return \yii\helpers\ArrayHelper::map($class::find()->asArray()->orderBy($displayField)->all(), 'id', $displayField);
    }
}