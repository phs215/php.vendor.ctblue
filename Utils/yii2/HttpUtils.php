<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/18/2017
 * Time: 4:21 PM
 */

namespace ctblue\web\Utils\yii2;


class HttpUtils
{
    public static function post($searchFilter, $parameter)
    {
        if ($p = \Yii::$app->request->post($searchFilter)) {
            if (isset($p[$parameter])) return $p[$parameter];
        }
        return false;
    }
    public static function get($searchFilter, $parameter)
    {
        if ($p = \Yii::$app->request->get($searchFilter)) {
            if (isset($p[$parameter])) return $p[$parameter];
        }
        return false;
    }
    public static function request($searchFilter, $parameter)
    {
        if ($p = \Yii::$app->request->get($searchFilter)) {
            if (isset($p[$parameter])) return $p[$parameter];
        }
        if ($p = \Yii::$app->request->post($searchFilter)) {
            if (isset($p[$parameter])) return $p[$parameter];
        }
        return false;
    }
}