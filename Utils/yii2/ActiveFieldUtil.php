<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/15/2017
 * Time: 1:34 AM
 */

namespace ctblue\web\Utils\yii2;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

class ActiveFieldUtil
{
    private static function getFmLocation()
    {
        return \Yii::getAlias('@web') . '/filemanager/';
    }

    /**
     * @param $form ActiveForm
     * @param $model ActiveRecord
     * @param $attribute string
     * @param $class ActiveRecord
     * @param $displayField string
     * @return string
     */
    public static function dropDown($form, $model, $attribute, $class, $displayField)
    {
        //get the class fields as array
        $array = ArrayHelper::map($class::find()->all(), 'id', $displayField);
        $data = $form->field($model, $attribute)->dropDownList($array,
            ['options' => [
                'class' => 'selectpicker',
            ],
            ]);
        return $data;
    }

    /**
     * @param $model ActiveRecord
     * @param $attribute string
     * @return string
     */
    public static function fileBrowser($model, $attribute)
    {
        $class = \yii\helpers\StringHelper::basename(get_class($model));
        $value = $model->{$attribute};
        $label = $model->attributeLabels()[$attribute];
        $data = '<div class="form-group ' . $attribute . ' required has-success">
<label class="control-label" for="' . $attribute . '">' . $label . '</label>
<input type="text" id="' . $attribute . '" class="form-control" name="' . $class . '[' . $attribute . ']" value="' . $value . '" maxlength="255" aria-required="true" aria-invalid="false">
<button class="btn btn-primary"
                            onclick="javascript:open_popup(\'' . ActiveFieldUtil::getFmLocation() . 'dialog.php?popup=1&field_id=' . $attribute . '\');return false;">
                        Choisir un fichier
                    </button>
<div class="help-block"></div>
</div>';
        return $data;
    }

    /**
     * @param $model ActiveRecord
     * @param $attribute string
     * @return string
     */
    public static function editor($model, $attribute)
    {
        $class = \yii\helpers\StringHelper::basename(get_class($model));
        $value = $model->{$attribute};
        $label = $model->attributeLabels()[$attribute];
        ob_start();
        ?>
        <div class="form-group">
            <label class="control-label" for="indexpageext-link_en"><?= $label ?></label>
            <textarea id="<?= $attribute ?>" name="<?= $class ?>[<?= $attribute ?>]"><?= $value ?></textarea>
            <div class="help-block"></div>
        </div>
        <script>
            $(document).ready(function () {
                tinymce.init({
                    selector: "#<?= $attribute?>",
                    extended_valid_elements: "i/em",
                    width: 900,
                    height: 300,
                    resize: "both",
                    theme: "modern",
                    fontsize_formats: "8pt 9pt 10pt 11pt 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
                    plugins: [
                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                        "insertdatetime media nonbreaking save table contextmenu directionality",
                        "emoticons template paste textcolor youtube"
                    ],
                    paste_auto_cleanup_on_paste: true,
                    paste_remove_spans: true,
                    paste_remove_styles: true,
                    paste_create_paragraphs: false,
                    paste_create_linebreaks: false,
                    paste_remove_styles_if_webkit: true,
                    paste_retain_style_properties: "none",
                    paste_strip_class_attributes: "all",
                    force_br_newlines: true,
                    force_p_newlines: false,
                    paste_text_sticky: true,
                    paste_text_sticky_default: true,
                    convert_urls: false,
                    entity_encoding: "raw",
                    paste_use_dialog: true,
                    forced_root_block: false, // Needed for 3.x
                    convert_newlines_to_brs: true,
                    invalid_elements: "p",
                    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                    toolbar2: "| responsivefilemanager | print preview media | forecolor backcolor emoticons youtube | fontselect | fontsizeselect | ltr rtl",
                    image_advtab: true,
                    external_filemanager_path: "<?= ActiveFieldUtil::getFmLocation()?>",
                    filemanager_title: "Responsive Filemanager",
                    filemanager_sort_by: "filename",
                    external_plugins: {"filemanager": "<?= ActiveFieldUtil::getFmLocation()?>/plugin.min.js"},
                    style_formats: [
                        {
                            title: "Interlignage", items: [
                            {title: "2", inline: 'div', styles: {'line-height': '2px'}}, {
                                title: "4",
                                inline: 'div',
                                styles: {'line-height': '4px'}
                            }, {title: "6", inline: 'div', styles: {'line-height': '6px'}}, {
                                title: "8",
                                inline: 'div',
                                styles: {'line-height': '8px'}
                            }, {title: "10", inline: 'div', styles: {'line-height': '10px'}}, {
                                title: "12",
                                inline: 'div',
                                styles: {'line-height': '12px'}
                            }, {title: "14", inline: 'div', styles: {'line-height': '14px'}}, {
                                title: "16",
                                inline: 'div',
                                styles: {'line-height': '16px'}
                            }, {title: "18", inline: 'div', styles: {'line-height': '18px'}}, {
                                title: "20",
                                inline: 'div',
                                styles: {'line-height': '20px'}
                            }, {title: "22", inline: 'div', styles: {'line-height': '22px'}}, {
                                title: "24",
                                inline: 'div',
                                styles: {'line-height': '24px'}
                            }, {title: "26", inline: 'div', styles: {'line-height': '26px'}}, {
                                title: "28",
                                inline: 'div',
                                styles: {'line-height': '28px'}
                            }, {title: "30", inline: 'div', styles: {'line-height': '30px'}}, {
                                title: "32",
                                inline: 'div',
                                styles: {'line-height': '32px'}
                            }, {title: "34", inline: 'div', styles: {'line-height': '34px'}}, {
                                title: "36",
                                inline: 'div',
                                styles: {'line-height': '36px'}
                            }, {title: "38", inline: 'div', styles: {'line-height': '38px'}}, {
                                title: "40",
                                inline: 'div',
                                styles: {'line-height': '40px'}
                            }, {title: "42", inline: 'div', styles: {'line-height': '42px'}}, {
                                title: "44",
                                inline: 'div',
                                styles: {'line-height': '44px'}
                            }, {title: "46", inline: 'div', styles: {'line-height': '46px'}}, {
                                title: "48",
                                inline: 'div',
                                styles: {'line-height': '48px'}
                            }, {title: "50", inline: 'div', styles: {'line-height': '50px'}}, {
                                title: "52",
                                inline: 'div',
                                styles: {'line-height': '52px'}
                            }, {title: "54", inline: 'div', styles: {'line-height': '54px'}}, {
                                title: "56",
                                inline: 'div',
                                styles: {'line-height': '56px'}
                            }, {title: "58", inline: 'div', styles: {'line-height': '58px'}}, {
                                title: "60",
                                inline: 'div',
                                styles: {'line-height': '60px'}
                            }, {title: "62", inline: 'div', styles: {'line-height': '62px'}}, {
                                title: "64",
                                inline: 'div',
                                styles: {'line-height': '64px'}
                            }, {title: "66", inline: 'div', styles: {'line-height': '66px'}}, {
                                title: "68",
                                inline: 'div',
                                styles: {'line-height': '68px'}
                            }, {title: "70", inline: 'div', styles: {'line-height': '70px'}}, {
                                title: "72",
                                inline: 'div',
                                styles: {'line-height': '72px'}
                            }, {title: "74", inline: 'div', styles: {'line-height': '74px'}}, {
                                title: "76",
                                inline: 'div',
                                styles: {'line-height': '76px'}
                            }, {title: "78", inline: 'div', styles: {'line-height': '78px'}}, {
                                title: "80",
                                inline: 'div',
                                styles: {'line-height': '80px'}
                            }, {title: "82", inline: 'div', styles: {'line-height': '82px'}}, {
                                title: "84",
                                inline: 'div',
                                styles: {'line-height': '84px'}
                            }, {title: "86", inline: 'div', styles: {'line-height': '86px'}}, {
                                title: "88",
                                inline: 'div',
                                styles: {'line-height': '88px'}
                            }, {title: "90", inline: 'div', styles: {'line-height': '90px'}}, {
                                title: "92",
                                inline: 'div',
                                styles: {'line-height': '92px'}
                            }, {title: "94", inline: 'div', styles: {'line-height': '94px'}}, {
                                title: "96",
                                inline: 'div',
                                styles: {'line-height': '96px'}
                            }, {title: "98", inline: 'div', styles: {'line-height': '98px'}},]
                        },
                        {
                            title: 'Headers', items: [
                            {title: 'Header 1', block: 'h1'},
                            {title: 'Header 2', block: 'h2'},
                            {title: 'Header 3', block: 'h3'},
                            {title: 'Header 4', block: 'h4'},
                            {title: 'Header 5', block: 'h5'},
                            {title: 'Header 6', block: 'h6'}
                        ]
                        },
                        {
                            title: 'Inline', items: [
                            {title: 'Bold', icon: "bold", inline: 'strong'},
                            {title: 'Italic', icon: "italic", inline: 'em'},
                            {
                                title: 'Underline',
                                icon: "underline",
                                inline: 'span',
                                styles: {'text-decoration': 'underline'}
                            },
                            {
                                title: 'Strikethrough',
                                icon: "strikethrough",
                                inline: 'span',
                                styles: {'text-decoration': 'line-through'}
                            },
                            {title: 'Superscript', icon: "superscript", inline: 'sup'},
                            {title: 'Subscript', icon: "subscript", inline: 'sub'},
                            {title: 'Code', icon: "code", inline: 'code'}
                        ]
                        },
                        {
                            title: 'Blocks', items: [
                            {title: 'Paragraph', block: 'p'},
                            {title: 'Blockquote', block: 'blockquote'},
                            {title: 'Div', block: 'div'},
                            {title: 'Pre', block: 'pre'}
                        ]
                        },
                        {
                            title: 'Alignment', items: [
                            {title: 'Left', icon: "alignleft", block: 'div', styles: {'text-align': 'left'}},
                            {title: 'Center', icon: "aligncenter", block: 'div', styles: {'text-align': 'center'}},
                            {title: 'Right', icon: "alignright", block: 'div', styles: {'text-align': 'right'}},
                            {title: 'Justify', icon: "alignjustify", block: 'div', styles: {'text-align': 'justify'}}
                        ]
                        }
                    ]
                });
            });
        </script>
        <?php
        $data = ob_get_clean();
        return $data;
    }
}