<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/15/2017
 * Time: 9:28 PM
 */

namespace ctblue\web\Utils\yii2;


use backend\modelsext\Userext;
use yii\helpers\Url;

class UserUtil
{
    public static function redirectIfNotLoggedIn()
    {
        if(\Yii::$app->user->isGuest){
            return \Yii::$app->getResponse()->redirect(Url::toRoute('site/login'));
            exit;
        }
    }
}