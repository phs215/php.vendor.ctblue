<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23-Aug-18
 * Time: 6:11 PM
 */

namespace ctblue\web\Utils\yii2;


use yii\helpers\Url;

class YiiUrlUtil
{
    public static function getFrontEndWebUrl($schema = '')
    {
        return str_replace('/backend/', '/frontend/', Url::base($schema));
    }

    public static function getBackendWebUrl($schema = '')
    {
        return str_replace('/frontend/', '/backend/', Url::base($schema));
    }

    public static function getFrontendDir()
    {
        $a= str_replace('\\backend', '\\frontend', \Yii::getAlias('@app'));
        $a= str_replace('\\console', '\\frontend', $a);
        $a= str_replace('/backend', '/frontend', $a);
        $a= str_replace('/console', '/frontend', $a);
        return $a;
    }
    public static function getBackendDir()
    {
        $a= str_replace('/frontend', '/backend', \Yii::getAlias('@app'));
        $a= str_replace('/console', '/backend', $a);
        return $a;
    }
}