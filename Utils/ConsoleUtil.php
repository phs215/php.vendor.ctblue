<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11-Nov-19
 * Time: 3:23 PM
 */

namespace ctblue\web\Utils;


class ConsoleUtil
{
    public static function stdout($message, $tabulations = 0, $newline = true)
    {
        for($i=0;$i<$tabulations;$i++){
            echo "&nbsp;";
        }
        echo $message;
        if($newline)echo "\r\n<br>";
    }
}