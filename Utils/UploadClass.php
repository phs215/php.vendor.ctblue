<?php
 namespace ctblue\web\Utils;

class UploadClass
{
    public static function UploadImage($post_name, $upload_to)
    {
        $allowedExts = array("gif", "jpeg", "jpg", "png", 'bmp');
        $temp = explode(".", $_FILES[$post_name]["name"]);
        $extension = end($temp);
        $upload_to .= '.' . $extension;
        if ((($_FILES[$post_name]["type"] == "image/gif")
                || ($_FILES[$post_name]["type"] == "image/jpeg")
                || ($_FILES[$post_name]["type"] == "image/bmp")
                || ($_FILES[$post_name]["type"] == "image/jpg")
                || ($_FILES[$post_name]["type"] == "image/pjpeg")
                || ($_FILES[$post_name]["type"] == "image/x-png")
                || ($_FILES[$post_name]["type"] == "image/png"))
            && ($_FILES[$post_name]["size"] < 1000000)
            && in_array(strtolower($extension), $allowedExts)
        ) {
            if ($_FILES[$post_name]["error"] > 0) {
                return false;
            } else {
                if (file_exists($upload_to)) {
                    return false;
                } else {
                    move_uploaded_file($_FILES[$post_name]["tmp_name"], $upload_to);
                    return $extension;
                }
            }
        } else {
            return false;
        }
    }

    public static function ResizeImage($filename, $extension, $newWidth = -1, $newHeight = -1, $proportional = true)
    {
        $fname = $filename . '.' . $extension;
        $size = getimagesize($fname);
        if ($proportional) {
            if ($size[0] > $size[1] || $newHeight == -1) {
                $ratio = $newWidth / $size[0];
                $newHeight = $size[1] * $ratio;
            } else {
                $ratio = $newHeight / $size[1];
                $newWidth = $size[0] * $ratio;
            }
        }
        self::ResizeBasic($fname, $extension, $filename . '_thumb.' . $extension, $newWidth, $newHeight);
//        $thumb = new Imagick($fname);
//        $thumb->resizeImage($newWidth,$newHeight,Imagick::FILTER_LANCZOS,1);
//        $thumb->writeImage($filename.'_thumb.'.$extension);
//        $thumb->destroy();
    }

    public static function ResizeBasic($image, $extension, $new_image, $width, $height)
    {
        $images_orig = null;
        switch (strtolower($extension)) {
            case 'gif':
                $images_orig = ImageCreateFromGif($image);
                break;
            case 'jpeg':
            case 'jpg':
                $images_orig = ImageCreateFromJpeg($image);
                break;
            case 'png':
                $images_orig = ImageCreateFromPng($image);
                break;
            case 'bmp':
                $images_orig = ImageCreateFromBmp($image);
                break;
        }

        $photoX = ImagesX($images_orig);
        $photoY = ImagesY($images_orig);
        $images_fin = ImageCreateTrueColor($width, $height);
        ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
        ImageJPEG($images_fin, $new_image);
        ImageDestroy($images_orig);
        ImageDestroy($images_fin);
    }
}