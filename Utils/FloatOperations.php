<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 10/2/2018
 * Time: 12:04 PM
 */

namespace ctblue\web\Utils;


class FloatOperations
{
    public static function isEqual($value1, $value2, $numberOfDecimals = 10)
    {
        return round($value1 - $value2, $numberOfDecimals) == 0;
    }
}