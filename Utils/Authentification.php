<?php
 namespace ctblue\web\Utils;

class Authentification
{
    public static function getCookie($name)
    {
        if (isset($_COOKIE[$name])) {
            if ($_COOKIE[$name] != "" && !empty($_COOKIE[$name])) {
                return self::replaceWYSIWYG($_COOKIE[$name]);
            }
        }
        return false;
    }

    public static function passwordHash($password,$salt)
    {
        return hash('sha256', $salt . hash('sha256', $password));
    }
    public static function passwordMD5($password,$salt)
    {
        return md5($password.$salt);
    }

    public static function createSalt()
    {
        $string = md5(uniqid(rand(), true));
        return substr($string, 0, 3);
    }

    public static function emailValidator($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL))
            return true;
        else
            return false;
    }
    public static function validateFullName($fullname){
        if (strpos(trim($fullname), ' ') !== true){
            return false;
        }
        return true;
    }
    public static function validateAlphanumeric($string){
        return ctype_alnum($string);
    }

    public static function passwordValidator($password)
    {
        if (strlen($password) >= 6)
            return true;
        else
            return false;
    }
}

?>