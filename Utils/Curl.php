<?php
 namespace ctblue\web\Utils;

class Curl {

    private $curl;

    public function Curl() {
        $this->curl = curl_init();
    }
	public function init(){
        $this->curl = curl_init();
	}
    public function Post($url, $parameters) {
        $ch=$this->curl;
//set POST variables
        foreach ($parameters as $key => $value) {
            $fields[$key] = urlencode($value);
        }
        $fields_string = "";
//url-ify the data for the POST
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        //rtrim($fields_string, '&');
//print $fields_string;
//open connection
//set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
       // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	  curl_setopt($ch, CURLOPT_AUTOREFERER , true);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
         curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIE , true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, "C:/wamp/www/realestate.com.lb/data/cookie.txt");
        curl_setopt($ch, CURLOPT_COOKIEFILE, "C:/wamp/www/realestate.com.lb/data/cookie.txt");
        //curl_setopt($ch, CURLOPT_HEADER, 1);
		//curl_setopt($ch,CURLOPT_ENCODING, "");
		curl_setopt($ch,CURLOPT_USERAGENT ,"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.8.1.7) Gecko/20070914 Firefox/2.0.0.7′");
		//curl_setopt($ch, CURLOPT_REFERER, $url);

//execute post
        $result = curl_exec($ch);
        return $result;
    }
	public function GetLastUrl(){
		return curl_getinfo($this->curl, CURLINFO_EFFECTIVE_URL);
	}

    public function Get($url) {
        $ch = $this->curl;
        curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
       // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIE , true);
       // curl_setopt($ch, CURLOPT_COOKIEJAR, "C:/wamp/www/ikarwbeit.com/data/cookie.txt");
        //curl_setopt($ch, CURLOPT_COOKIEFILE, "C:/wamp/www/ikarwbeit.com/data/cookie.txt");
        curl_setopt($ch, CURLOPT_COOKIEJAR, "C:/wamp/www/realestate.com.lb/data/cookie.txt");
        curl_setopt($ch, CURLOPT_COOKIEFILE, "C:/wamp/www/realestate.com.lb/data/cookie.txt");
        //curl_setopt($ch, CURLOPT_HEADER, 1);
// Send the request & save response to $resp
        $resp = curl_exec($ch);
        return $resp;
    }

    public function Close() {
        curl_close($this->curl);
    }

}

?>