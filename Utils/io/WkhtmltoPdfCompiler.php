<?php


namespace ctblue\web\Utils\io;


class WkhtmltoPdfCompiler
{
    private $htmlFilename;
    private $pdfFilename;
    public $marginBottom = 10;
    public $marginTop = 15;
    public $marginLeft = 10;
    public $marginRight = 10;
    public $spacingBottom = 20;
    public $footerFontSize = 8;
    public $pageSize = 'A4';
    public $paginationText = 'Page [page] of [toPage]';

    function __construct($pdfFilename, $htmlFilename = false)
    {
        $this->htmlFilename = $htmlFilename;
        if (!$htmlFilename) {
            //create a temporary html file
            $this->htmlFilename = tempnam(sys_get_temp_dir(), 'html');
        }
        $this->pdfFilename = $pdfFilename;
    }

    public function openPdf()
    {
        return \Yii::$app->response->sendFile($this->pdfFilename, basename($this->pdfFilename), ['inline' => true]);
    }

    public function writeHtml($html)
    {
        if ($html) {
            file_put_contents($this->htmlFilename, $html);
        }
    }

    public function buildHtml($originalHtmlFile, $variables)
    {
        if (file_exists($originalHtmlFile)) {
            $content = file_get_contents($originalHtmlFile);
            foreach ($variables as $key => $value) {
                $content = str_replace('{' . $key . '}', "$value", $content);
            }
//            echo $content;
//            exit;
            $this->writeHtml($content);
        }
    }

    public function compile($wkhtmltopdfLocation)
    {
        $cmd = $wkhtmltopdfLocation;
        $cmd .= '  --footer-center ';
        if ($this->paginationText) {
            $cmd .= '"Page [page] of [toPage]"';
        }
        $cmd .= ' --footer-font-size ' . $this->footerFontSize . ' --page-size ' . $this->pageSize . ' --margin-bottom ' . $this->marginBottom . 'mm --margin-top ' . $this->marginTop . 'mm --margin-right ' . $this->marginRight . 'mm --footer-spacing ' . $this->spacingBottom . ' --margin-left ' . $this->marginLeft . 'mm --disable-smart-shrinking --enable-javascript "' . $this->htmlFilename . '"' . ' "' . $this->pdfFilename . '"';
//        echo $cmd;exit;
        exec($cmd, $output);
//        var_dump($output);
//        exit;
        if (file_exists($this->htmlFilename)) unlink($this->htmlFilename);
        if (file_exists($this->pdfFilename)) {
            return true;
        } else {
            return false;
        }
    }
}