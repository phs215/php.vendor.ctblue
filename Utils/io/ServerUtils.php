<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 10/23/2017
 * Time: 1:46 PM
 */

namespace ctblue\web\Utils\io;


class ServerUtils
{
    public static function availableUrl($host, $port=80, $timeout=10) {
        set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
            throw new \ErrorException("Error");
        });
        try {
            $fp = fSockOpen($host, $port, $errno, $errstr, $timeout);
            return $fp!=false;
        } catch (\ErrorException $e) {
//            echo 'Error';
        }
        restore_error_handler();
        return false;
    }
}