<?php
namespace ctblue\web\Utils\DebugTools;


use ctblue\web\Utils\DateTimeUtils;
use ctblue\web\Utils\StringUtils;

class DbCommands
{
    public static $command_time = array();
    public static $DEBUG=false;
    public static function setStartTime($cmd)
    {
        if(self::$DEBUG){
            $rand = StringUtils::Random();
            self::$command_time[$cmd . ' - Id: ' . $rand] = DateTimeUtils::getCurrentTimeStamp();
            return $rand;
        }
    }
    /**
     * gets the php time in seconds
     * @param $cmd
     * @param $random_id
     */
    public static function setEndTime($cmd, $random_id)
    {
        if(self::$DEBUG){
            self::$command_time[$cmd . ' - Id: ' . $random_id] = DateTimeUtils::getCurrentTimeStamp() - self::$command_time[$cmd . ' - Id: ' . $random_id];
        }
    }
    public static function getQueriesWithTimeList()
    {
        \Kint::dump(self::$command_time);
//        if(self::$DEBUG){
//            $output = '';
//            foreach (self::$command_time as $k => $v) {
//                $output .= $k . ' : ' . $v . '<br />';
//            }
//            return $output;
//        }
    }
}