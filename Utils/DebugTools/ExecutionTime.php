<?php
namespace ctblue\web\Utils\DebugTools;


class ExecutionTime extends DebugAbstract
{
    private $local_start_time = '';
    private static $start_time = '';

    public function SetLocalStartTime()
    {
        $this->local_start_time = microtime(true);
    }

    public function GetLocalStartTime()
    {
        $time_end = microtime(true);
        $execution_time = ($time_end - $this->local_start_time);
        return '<b>Total Execution Time:</b> ' . $execution_time . ' Seconds';
    }

    public static function SetGlobalStartTime()
    {
        self::$start_time = microtime(true);
    }

    public static function GetGlobalExecutionTime()
    {
        $time_end = microtime(true);
        $execution_time = ($time_end - self::$start_time);
        return $execution_time;
    }

    public function PrintDebugData()
    {
        $time = ExecutionTime::GetGlobalExecutionTime();
        echo '<b>Total Execution Time:</b> ' . $time . ' Seconds';
    }
}