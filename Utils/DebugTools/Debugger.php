<?php

namespace ctblue\web\Utils\DebugTools;


class Debugger
{
    public function ShowInfo()
    {
        ?>
        <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
        <script type="text/javascript">
            var top_window = (window.self != window.parent) ? window.parent : window.parent;
            top_window.resizeWrapperdebug=function(height) {
                top_window.$('body').find('#wrapper').find('#pushdebug').remove();
                top_window.$('body').find('#wrapper').append('<div id="pushdebug" style="height:' + height + 'px;width:100%;"></div>');
                top_window.$('body').find('#wrapper').css({'margin': 'margin:0 auto -' + height + 'px'});
            };
            top_window.fullScreendebug=function() {
                top_window.$('body').find('#wrapper').find('#pushdebug').remove();
                var height = $(window).height();
                top_window.$('#debug_trace').css({"height": height + "px"});
            };
            top_window.halfScreendebug=function() {
                top_window.$('body').find('#wrapper').find('#pushdebug').remove();
                var height = $(window).height();
                top_window.$('#debug_trace').css({"height": (height / 2) + "px"});
                top_window.resizeWrapperdebug(height / 2);
            };
            top_window.minimizedebug=function() {
                top_window.$('body').find('#wrapper').find('#pushdebug').remove();
                top_window.$('#debug_trace').css({"height": 100 + "px"});
                top_window.resizeWrapperdebug(100);
            };
            top_window.closeDebug=function() {
                top_window.$('body').find('#wrapper').find('#pushdebug').remove();
                top_window.$('#debug_trace').css({"height": 20 + "px"});
                top_window.resizeWrapperdebug(20);
            };
            $(document).ready(function(){
                var data = $('#debug_trace_hidden').html();
                $('#debug_trace_hidden').remove();
                top_window.$('body').find('#debug_trace').remove();
                top_window.$('body').append(data);
                top_window.$('body').find('#pushdebug').remove();
                top_window.showTab = function (tab) {
                    for (var i = 0; i < tabs.length; i++) {
                        top_window.$('#debug_trace').find('.' + tabs[i]).hide();
                    }
                    top_window.$('#debug_trace').find('.' + tab).show();
                    top_window.$('.kint-parent').click();
                };
                top_window.minimizedebug();
            });
        </script>
        <div style="display:none;resize:both;" id="debug_trace_hidden">
            <div id="debug_trace"
                 style="position:fixed;height:100px;bottom:0;left:0;background-color:white;border-top:1px solid black;width:100%;z-index:100000;display:inline;overflow-y:scroll;color:black;font-size:12px;">
                <?php
                //start header
                echo '<div style="width:100%;height:20px;">';
                $tabs = ['Execution_time', 'StackTrace', 'Global Vars', 'Database Commands'];
                $tabs_lower = [];
                foreach ($tabs as $t) {
                    $t1 = str_replace(' ', '_', strtolower($t));
                    $tabs_lower[] = $t1;
                    ?>
                    <div style="float:left;" onclick="showTab('<?php echo $t1 ?>')"
                         class="m_over"><?php echo $t ?></div>
                    <div style="float:left;">&nbsp;&nbsp;</div>
                    <?php
                }
                ?>
                <div style="float:right;margin-left:10px" onclick="closeDebug()">X</div>
                <div style="float:right;margin-left:10px" onclick="fullScreendebug()">F</i>
                </div>
                <div style="float:right;margin-left:10px" onclick="halfScreendebug()">+</div>
                <div style="float:right;margin-left:10px" onclick="minimizedebug()">-</div>
                <?php
                echo '</div><div class="clear"></div>';
                //end header
                echo '<div style="display:block;" class="execution_time">';
                $exec = new ExecutionTime();
                $exec->PrintDebugData();
                echo '</div>';

                echo '<div style="display:none;" class="stacktrace">';
                debug_print_backtrace();
                echo '</div>';

                echo '<div style="display:none;" class="global_vars">';
                $v = new GlobalVars();
                $v->PrintDebugData();
                echo '</div>';

                echo '<div style="display:none;" class="database_commands">';
                echo DbCommands::getQueriesWithTimeList();
                echo '</div>';
                ?>
                <script type="text/javascript">
                    var tabs = [];
                    <?php
                    for($i=0;$i<count($tabs_lower);$i++){
                        echo 'tabs['.$i.']=\''.$tabs_lower[$i].'\';';
                    }
                    ?>
                </script>
            </div>
        </div>
        <?php
    }
}