<?php
namespace ctblue\web\Utils\DebugTools;


abstract class DebugAbstract
{
    public abstract function PrintDebugData();
}