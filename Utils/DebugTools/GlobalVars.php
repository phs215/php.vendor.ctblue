<?php
/**
 * Created by PhpStorm.
 * User: Philippe
 * Date: 11/14/2015
 * Time: 2:34 AM
 */

namespace ctblue\web\Utils\DebugTools;


class GlobalVars extends DebugAbstract
{

    public function PrintDebugData()
    {
        \Kint::dump($GLOBALS);
    }
}