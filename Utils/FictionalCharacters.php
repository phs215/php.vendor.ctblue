<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 11/9/2017
 * Time: 12:31 PM
 */

namespace ctblue\web\Utils;


class FictionalCharacters
{
    private static $list = ['Bugs Bunny','Harry Potter','Tom','Jerry','Pink Panter','Mickey Mouse','Minnie','Dingo','Asterix','Obelix','Lucky Luke'];

    public static function getOne()
    {
        return FictionalCharacters::$list[array_rand(FictionalCharacters::$list)];
    }
}