<?php

namespace ctblue\web\Utils;

class StringUtils
{
    public static function Rand($loop = 3)
    {
        return self::Random($loop);
    }

    public static function delete_all_between($beginning, $end, $string)
    {
        $beginningPos = strpos($string, $beginning);
        $endPos = strpos($string, $end);
        if ($beginningPos === false || $endPos === false) {
            return $string;
        }

        $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

        return str_replace($textToDelete, '', $string);
    }

    public static function Random($loop = 3)
    {
        $string = "";
        for ($i = 0; $i < $loop; $i++) {
            $a = rand(0, 1);
            if ($a == 0) {
                $string .= chr(rand(97, 122));
            } else {
                $string .= rand(1, 9);
            }
        }
        return $string;
    }

    public static function UniqueID($more_entropy = true)
    {
        return md5(uniqid(rand(), $more_entropy));
    }

    public static function RemoveSingleAlphabet($string)
    {
        $c = 'a';
        // print $string[0].$string[1];
        while ($c >= 'a' && $c <= 'z') {
            $string = self::replaceFromStart($string, $c . ' ');
            $string = self::replaceFromStart($string, strtoupper($c) . ' ');
            $string = str_replace(' ' . $c . ' ', '', $string);
            $string = str_replace(' ' . strtoupper($c) . ' ', '', $string);
            if ($c == 'z') break;
            $c++;
        }
        return $string;
    }

    public static function SplitOnWord($string, $your_desired_width)
    {
        $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
        $parts_count = count($parts);
        $length = 0;
        $last_part = 0;
        for (; $last_part < $parts_count; ++$last_part) {
            $length += strlen($parts[$last_part]);
            if ($length > $your_desired_width) {
                break;
            }
        }
        return implode(array_slice($parts, 0, $last_part));
    }

    public static function removeNLCustom($string)
    {
        $string = str_replace("\r\n", "", $string);
        $string = str_replace("\t", "", $string);
        $string = str_replace("\r", "", $string);
        $string = str_replace("\n", "", $string);
        return $string;
    }

    public static function removeNL($string)
    {
        return $string = trim(preg_replace('/\s+/', ' ', $string));
    }

    public static function replaceFromStart($string, $search)
    {
        $prefix = $search;
        $str = $string;
        if (substr($str, 0, strlen($prefix)) == $prefix) {
            $str = substr($str, strlen($prefix));
        }
        return $str;
    }
    public static function removeDoubleSpaces($string){
        return preg_replace('!\s+!', ' ', $string);
    }
    public static function removeFromEnd($string, $search)
    {
        $prefix = $search;
        $str = $string;
        if (StringUtils::endsWith($string, $search)) {
            return substr($str, 0, -strlen($prefix));;
        }
        //echo substr($str, 0, -strlen($prefix));
        if (substr($str, 0, -strlen($prefix)) == $prefix) {
            $str = substr($str, strlen($prefix));
        }
        return $str;
    }

    public static function GetStringBetween($before, $after, $string)
    {
        $tmp = explode($before, $string);
        //print_r($tmp);
        $found = false;
        if (sizeof($tmp) > 1) {
            for ($i = 1; $i < sizeof($tmp); $i++) {
                if (strpos($tmp[$i], $after) !== false) {
                    //print $tmp[$i];
                    $tmp = explode($after, $tmp[$i]);
                    $found = true;
                    break;
                }
            }
            // print_r($tmp);
        } else return "";
        if (!$found) return '';
        return $tmp[0];
    }

    public static function deleteFirstBetween($beginning, $end, $string)
    {
        $beginningPos = strpos($string, $beginning);
        $endPos = strpos($string, $end);
        if ($beginningPos === false || $endPos === false) {
            return $string;
        }
        $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);
        return str_replace($textToDelete, '', $string);
    }

    public static function deleteAllBetween($beginning, $end, $string)
    {
        $a = substr_count($string, $beginning);
        $b = substr_count($string, $end);
        $a = ($a < $b) ? $a : $b;
        for ($i = 0; $i < $a; $i++) {
            $string = self::deleteFirstBetween($beginning, $end, $string);
        }
        return $string;
    }

    public static function GetAllStringsBetween($before, $after, $string)
    {
        $tmp = explode($before, $string);
        //print_r($tmp);
        $res = array();
        for ($i = 1; $i < sizeof($tmp); $i++) {
            $a = explode($after, $tmp[$i]);
            if (isset($a[0])) {
                $res[] = $a[0];
            }
        }
        return $res;
    }

    /**
     * string starts with a certain string
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function startsWith($haystack, $needle)
    {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }

    /**
     * string ends with a certain string
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function endsWith($haystack, $needle)
    {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
    }

    /**
     * Checks if the haystack ends with in any of the strings in the array
     * @param $haystack
     * @param $needleArray
     * @return bool
     */
    public static function endsWithAny($haystack, $needleArray)
    {
        foreach ($needleArray as $value) {
            if (StringUtils::endsWith($haystack, $value)) return true;
        }
        return false;
    }

    /**
     * Checks if the haystack starts with in any of the strings in the array
     * @param $haystack
     * @param $needleArray
     * @return bool
     */
    public static function startsWithAny($haystack, $needleArray)
    {
        foreach ($needleArray as $value) {
            if (StringUtils::startsWith($haystack, $value)) return true;
        }
        return false;
    }

    /**
     * Removes the first character from the string
     * @param $string
     * @return string
     */
    public static function removeFirstChar($string)
    {
        return substr($string, 1);
    }

    public static function ellipsis($in, $length)
    {
        return strlen($in) > $length ? substr($in, 0, $length) . "..." : $in;
    }

    public static function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Searches for a needle inside the haystack
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function contains($haystack, $needle)
    {
        if (!$needle) return false;
        if (strpos($haystack, $needle) === false) return false;
        return true;
    }

    /**
     * Checks if the haystack is contained in any of the strings in the haystack
     * @param $haystack
     * @param $needleArray
     * @return bool
     */
    public static function containsAny($haystack, $needleArray)
    {
        foreach ($needleArray as $value) {
            if (StringUtils::contains($haystack, $value)) return true;
        }
        return false;
    }

    /**
     * Checks if the haystack is equal to in any of the strings in the haystack
     * @param $haystack
     * @param $needleArray
     * @return bool
     */
    public static function isEqualAny($haystack, $needleArray)
    {
        foreach ($needleArray as $value) {
            if ($value === $haystack) return true;
        }
        return false;
    }

    public static function removeLastChar($string)
    {
        return substr($string, 0, -1);
    }

    public static function removeSpacesNewLinesBreaks($string)
    {
        return trim(preg_replace('/\s+/', ' ', $string));
    }

    /**
     * @param $string string
     * @param $needles string[]
     * @param $replaceWith string
     * @return mixed
     */
    public static function replaceAny($string, $needles, $replaceWith)
    {
        foreach ($needles as $needle) {
            $string = str_replace($string, $needle, $replaceWith);
        }
        return $string;
    }
}
