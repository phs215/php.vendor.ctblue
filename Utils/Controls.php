<?php
namespace ctblue\web\Utils;


use ctblue\web\DAO\Databases\Database;

class Controls
{

    public static function WYSIWYG($name, $data)
    {
        $tmp = "";
        $tmp .= '<textarea id="' . $name . '" name="' . $name . '" >' . $data .
            '</textarea>';
        $tmp .= '<script type="text/javascript">
$(document).ready(function(){
	CKEDITOR.replace( "' . $name . '" ,
    {
	filebrowserBrowseUrl : "' . $GLOBALS['wysiwyg'] . "&ckeditor=true" . '",
	filebrowserImageBrowseUrl : "' . $GLOBALS['wysiwyg'] . "&ckeditor=true" . '",
	filebrowserFlashBrowseUrl : "' . $GLOBALS['wysiwyg'] . "&ckeditor=true" . '",
	filebrowserUploadUrl : "' . $GLOBALS['wysiwyg'] . "&ckeditor=true" . '",
	filebrowserImageUploadUrl : "' . $GLOBALS['wysiwyg'] . "&ckeditor=true" . '",
	filebrowserFlashUploadUrl : "' . $GLOBALS['wysiwyg'] . "&ckeditor=true" . '",
	toolbar:"Full"
});
});
</script>';
        return $tmp;
    }

    public static function TimeJqueryUI($name, $time = "", $script = "", $required = false)
    {
        $tmp = '
<input id="' . $name . '" name="' . $name . '" type="text" ';
        if ($required) {
            $tmp .= ' data-validation="required" ';
        }
        $tmp .= ' autocomplete="off">';
        $tmp .= '<script type="text/javascript">
$(document).ready(function(){
var obj=jQuery("#' . $name . '");
		obj.timepicker({
			showCloseButton: true,
			showNowButton: true
			});
			obj.val("' . $time . '");
                            ' . $script . '
	});
	</script>
';
        return $tmp;
    }

    public static function DateJqueryUI($name, $disable = false, $date = "", $width = "", $required = false)
    {
        $tmp = '
<input id="' . $name . '" name="' . $name . '" type="text" autocomplete="off"';
        if ($required) {
            $tmp .= ' data-validation="required" ';
        }
        if ($width != "") {
            $tmp .= " style='width:" . $width . "px' ";
        }
        $tmp .= '>
<script type="text/javascript">
$(document).ready(function() {
		jQuery( "#' . $name . '" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
        $( "#' . $name . '" ).datepicker( "option", "dateFormat", "dd-mm-yy" );
        $( "#' . $name . '" ).datepicker("setDate","' . $date . '");
	});
	</script>';
        return $tmp;
    }
    public static function DateBootstrap($name,$date='',$width='300')
    {
        ob_start();
        if($date=='now'){
            $date=date('m/d/Y');
        }
        ?>
        <div class="input-group date" data-provide="datepicker" style="float:left;width:<?php echo $width?>px">
            <input type="text" class="form-control" name="<?php echo $name?>" value="<?php echo $date?>">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.datepicker').datepicker({
                    format: 'mm/dd/yyyy'
                })
            });
        </script>
        <?php
        return ob_get_clean();
    }

    public static function DateTed($name, $date = "", $width = "")
    {
        $tmp = '
		<script type="text/javascript" src="js/cal.js"></script>
		<link href="css/calendar.css" rel="stylesheet" type="text/css" />
 <input type="text" name="' . $name . '" id="' . $name . '" ';
        if ($width != "") {
            $tmp .= " style='width:" . $width . "px' ";
        }
        $tmp .= ' autocomplete="off" value="' . $date . '"/>
 <script type="text/javascript">
 jQuery(\'#' . $name . '\').simpleDatepicker({ chosendate:"' . $date . '",startdate: 1900, enddate: 2012 });
 </script>
';
        return $tmp;
    }

    public static function ColorPicker($name, $data, $required = false)
    {
        ob_start();
        ?>
        <input type="text" <?php if ($required) echo ' data-validation="required" ' ?> autocomplete="off"
               name="<?php print $name ?>" id="<?php print $name ?>"
               value="<?php print $data ?>"/>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#<?php print $name ?>').ColorPicker({
                        onSubmit: function (hsb, hex, rgb, el) {
                            $(el).val("#" + hex);
                            $(el).ColorPickerHide();
                        },
                        onBeforeShow: function () {
                            $(this).ColorPickerSetColor(this.value);
                        }
                    })
                    .bind('keyup', function () {
                        $(this).ColorPickerSetColor(this.value);
                    });
            });
        </script>
        <?php
        return ob_get_clean();
    }

    public static function printDate($year = "", $month = "", $day = "")
    {
        ?>

        Day:
        <select name="day">
            <?php
            $a = true;
            for ($i = 1; $i < 32; $i++) {
                if ($day == $i) {
                    $a = false;
                    print '<option value="' . $i . '" selected="selected">' . $i . '</option>';
                } else
                    if ($i == date('j') && $a) {
                        print '<option value="' . $i . '" selected="selected">' . $i . '</option>';
                    } else {
                        print '<option value="' . $i . '">' . $i . '</option>';
                    }
            }
            ?>
        </select>
        &nbsp;&nbsp;
        Month:
        <select name="month">
            <?php
            $a = true;
            for ($i = 1; $i < 13; $i++) {
                if ($month == $i) {
                    $a = false;
                    print '<option value="' . $i . '" selected="selected">' . $i . '</option>';
                } else
                    if ($i == date('n') && $a) {
                        print '<option value="' . $i . '" selected="selected">' . $i . '</option>';
                    } else {
                        print '<option value="' . $i . '">' . $i . '</option>';
                    }
            }
            ?>
        </select>
        &nbsp;&nbsp;
        Year:
        <select name="year">
            <?php
            for ($i = date('Y'); $i >= 2000; $i--) {
                if ($year == $i) {
                    print '<option value="' . $i . '" selected="selected">' . $i . '</option>';
                } else
                    print '<option value="' . $i . '">' . $i . '</option>';
            }
            ?>
        </select>
        <?php
    }

    /**
     * @param $db Database
     * @param $name
     * @param string $selectedCountry
     * @param string $width
     * @param string $bootstrap
     * @param string $padding
     * @return string
     */
    public static function printCountries($db, $name, $selectedCountry = "", $width = "", $bootstrap = "", $padding = "")
    {
        if ($bootstrap != "") {
            global $plugindir;
            include($plugindir . '/bootstrap-select/index.php');
        }
        $cmd = "SELECT * FROM country";
        $tmp = "";
        if ($result = $db->returnFromDB($cmd)) {
            //print_r($result);
            $tmp .= '<select name="' . $name . '" id="' . $name . '" ';
            if ($bootstrap != "") {
                $tmp .= ' class="selectpicker" data-style="' . $bootstrap . '" ' . $padding . ' ';
            }
            if ($width != "") {
                $tmp .= " style='width:" . $width . "px' ";
            }
            $tmp .= '>';
            foreach ($result as $country) {
                if ($selectedCountry == $country['id']) {
                    $tmp .= '<option value="' . $country['id'] . '" selected="selected">' . $country['countries_name'] . '</option>';
                } else {
                    $tmp .= '<option value="' . $country['id'] . '">' . $country['countries_name'] . '</option>';
                }
            }
            $tmp .= '</select>';
        }
        return $tmp;
    }

}

?>
