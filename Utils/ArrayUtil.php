<?php


namespace ctblue\web\Utils;


class ArrayUtil
{
    public static function arrayToObject($className, $array)
    {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(serialize($array), ':')
        ));
    }

    public static function split($array, $slices)
    {
        $chunks = [];
        if (!isset($array[0])) {
            return $array;
        }
        $len = count($array);
        return array_chunk($array, ceil($len / 3));
    }
    public static function flatten(array $array) {
        $output = [];
        array_walk_recursive($array, function($a) use (&$output) { $output[] = $a; });
        return $output;
    }
}
