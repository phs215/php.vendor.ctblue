<?php
namespace ctblue\web\Utils\encryptions;

class OpenSSL
{
    public $method = 'aes-256-cbc';
    public $password = '';
    public $iv='';

    function __construct($password)
    {
        $this->password = $password;
    }

    public function encrypt($str)
    {
        if($this->iv!=''){
            $res = openssl_encrypt($str, $this->method, $this->password,false, $this->iv);
        }else{
            $res = openssl_encrypt($str, $this->method, $this->password);
        }
        return $res;
    }

    public function decrypt($str)
    {
        if (!function_exists('openssl_decrypt')) {
//            echo 'openssl decrypt function does not exist in the current server configuration';
//            exit;
            return false;
        }
        if($this->iv!=''){
            $res = openssl_decrypt($str, $this->method, $this->password, 0, $this->iv);
        }else{
            $res = openssl_decrypt($str, $this->method, $this->password);
        }
        if (!$res) {
//            echo 'there is an error in the openssl_decrypt function';
            return false;
        }
        return $res;
    }
}