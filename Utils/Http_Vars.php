<?php
namespace ctblue\web\Utils;


class Http_Vars
{
    public static $G = array();
    public static $P = array();

    public static function ReadAll()
    {
        if (isset($_GET)) self::$G = $_GET;
        if (isset($_POST)) self::$P = $_POST;
    }

    public static function Replace($var, $value, $get = true)
    {
        $vars = self::$G;
        $vars[$var] = $value;
        if ($get) {
            self::$G = $vars;
        } else {
            self::$P = $vars;
        }
    }

    public static function Remove($url, $var)
    {
        //Instead of hacking around with regular expression you should parse the string as an url (what it is)
        $string = $url;
        $parts = parse_url($string);
        $queryParams = array();
        if(isset($parts['query'])){
            parse_str($parts['query'], $queryParams);
            //Now just remove the parameter
            unset($queryParams[$var]);
        }
        //and rebuild the url
        $url = $parts['path'] . '?' . http_build_query($queryParams);;
        return $url;
    }

    public static function GetUrl($get = true)
    {
        if ($get) {
            $vars = self::$G;
        } else {
            $vars = self::$P;
        }
        $i = 0;
        $url = "";
        foreach ($vars as $key => $value) {
            if (is_array($value)) {
                $c = count($value);
                for ($j = 0; $j < $c; $j++) {
                    if ($i == 0) {
                        $url .= "?" . $key . "[]=" . urlencode($value[$j]);
                    } else {
                        $url .= "&" . $key . "[]=" . urlencode($value[$j]);
                    }
                    $i++;
                }
            } else {
                if ($i == 0) {
                    $url .= "?" . $key . "=" . urlencode($value);
                } else {
                    $url .= "&" . $key . "=" . urlencode($value);
                }
                $i++;
            }
        }
        return $url;
    }

    public static function location($location)
    {
        header('Location:' . $location);
        ob_end_flush();
    }

    public static function quit()
    {
        ob_end_flush();
        exit();
    }

    public static function Get($get)
    {
        return self::VerifyGet($get);
    }

    public static function VerifyGet($get)
    {
        if (isset($_GET[$get])) {
            if ($_GET[$get] != '' && !empty($_GET[$get])) {
                return $_GET[$get];
            } else
                return false;
        } else
            return false;
    }

    public static function Files($get)
    {
        if (isset($FILES[$get])) {
            if ($FILES[$get] != '') {
                return $FILES[$get];
            }
        }
        return false;
    }

    public static function Post($get, $empty_allowed = false)
    {
        return self::VerifyPost($get, $empty_allowed);
    }

    public static function VerifyPost($get, $empty_allowed = false)
    {
        if (isset($_POST[$get])) {
            if ($_POST[$get] != '') {
                return $_POST[$get];
            } else if ($_POST[$get] == '' && $empty_allowed) {
                return '';
            }
        }
        return false;
    }

    public static function Cookie($get)
    {
        if (isset($_COOKIE[$get])) {
            if ($_COOKIE[$get] != '' && !empty($_COOKIE[$get])) {
                return $_COOKIE[$get];
            }
        }
        return false;
    }

    public static function Header($get)
    {
        $headers = apache_request_headers();
        if (isset($headers[$get])) {
            if ($headers[$get] != '' && !empty($headers[$get])) {
                return $headers[$get];
            }
        }
        return false;
    }

    public static function VerifySession($get, $get1 = "")
    {
        if ($get1 != "") {
            if (isset($_SESSION[$get][$get1])) {
                if ($_SESSION[$get][$get1] != '' && !empty($_SESSION[$get][$get1])) {
                    return $_SESSION[$get][$get1];
                }
            }
            return false;
        }
        if (isset($_SESSION[$get])) {
            if ($_SESSION[$get] != '' && !empty($_SESSION[$get])) {
                return $_SESSION[$get];
            }
        }
        return false;
    }

    public static function Session($get, $get1 = "")
    {
        return self::VerifySession($get, $get1);
    }

    public static function VerifyRequest($get)
    {
        if (isset($_REQUEST[$get])) {
            if ($_REQUEST[$get] != '' && !empty($_REQUEST[$get])) {
                return $_REQUEST[$get];
            }
        }
        return false;
    }

    public static function getValue($get)
    {
        if (($a = self::VerifyPost($get)))
            return $a;
        return '';
    }

    public static function printError($cmd)
    {
        self::echoError($cmd);
    }

    public static function echoError($cmd)
    {
        return '<span style="color:red">' . $cmd . '</span>';
    }

    public static function Redirect($url = '')
    {
        if ($url == '') {
            $url = $_SERVER['REQUEST_URI'];
        }
        header('HTTP/1.1 303 See Other');
        header('Location:' . $url);
        exit();
    }
}

Http_Vars::ReadAll();
?>