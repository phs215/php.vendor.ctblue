<?php
namespace ctblue\web\Utils;


class JSON
{
    public static function json_encode($a = false)
    {
        if (is_null($a)) return 'null';
        if ($a === false) return 'false';
        if ($a === true) return 'true';
        if (is_scalar($a)) {
            if (is_float($a)) {
                // Always use "." for floats.
                return floatval(str_replace(",", ".", strval($a)));
            }

            if (is_string($a)) {
                static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
                return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
            } else
                return $a;
        }
        $isList = true;
        for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
            if (key($a) !== $i) {
                $isList = false;
                break;
            }
        }
        $result = array();
        if ($isList) {
            foreach ($a as $v) $result[] = json_encode($v);
            return '[' . join(',', $result) . ']';
        } else {
            foreach ($a as $k => $v){
                $key=json_encode($k);
                if(is_numeric($key))$key='"'.$key.'"';
                $result[] = json_encode($key) . ':' . json_encode($v);
            }
            return '{' . join(',', $result) . '}';
        }
    }

    public static function TableToJSON($table, $array)
    {
        //get data from table
        $cols = Database::GetColumns($table);
        $out = "";
        $co=sizeof($array);
        $co1=sizeof($cols);
        for ($j = 0; $j < $co; $j++) {
            $r = $array[$j];
            $out .= '{';
            for ($i = 0; $i < $co1; $i++) {
                $c = $cols[$i];
                $out .= JSON::json_encode($c) . ':' . JSON::json_encode($r[$c]);
                if ($i < sizeof($cols) - 1) {
                    $out .= ',';
                }
            }
            $out .= '}';
            if ($j < $co - 1) {
                $out .= ',';
            }
        }
        return '[' . $out . ']';
    }

    public static function ColsToJSON($cols, $array)
    {
        //get data from table
        $out = "";
        $co=sizeof($array);
        $co1=sizeof($cols);
        for ($j = 0; $j < $co; $j++) {
            $r = $array[$j];
            $out .= '{';
            for ($i = 0; $i < $co1; $i++) {
                $c = $cols[$i];
//                $out .= JSON::json_encode($c) . ':' . JSON::json_encode($r[$c]);
                $out .= JSON::json_encode('x' . $i) . ':' . JSON::json_encode($r[$c]);
                if ($i < $co1 - 1) {
                    $out .= ',';
                }
            }
            $out .= '}';
            if ($j < $co - 1) {
                $out .= ',';
            }
        }
        return '[' . $out . ']';
    }
} 