<?php

namespace ctblue\web\auth\yii2;


class AdAuthorize
{
    public static function login($adUsername, $adPassword, $autorizationUrl)
    {
        $userTable = 'user';
        if (isset(\Yii::$app->params['userTable']) && ($x = \Yii::$app->params['userTable'])) {
            $userTable = $x;
        }
//        if (!$autorizationUrl) return false;
        $db = \Yii::$app->db;
        //check if the user has been found in the database
        if (
            $db->createCommand("SHOW COLUMNS FROM `$userTable` LIKE 'ad_username'")->queryOne()
            && $db->createCommand("SHOW COLUMNS FROM `$userTable` LIKE 'active'")->queryOne()
            && ($res = $db->createCommand("SELECT * FROM $userTable WHERE ad_username=:u AND active=1", [':u' => $adUsername])->queryOne())
        ) {
            if (isset($res['id'])) {
                $id = $res['id'];
//                echo $autorizationUrl;exit;
                if ($autorizationUrl) {
                    $vars = [
                        'username' => $adUsername,
                        'password' => $adPassword
                    ];
                    try {
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $autorizationUrl);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($vars));
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $server_output = curl_exec($ch);
                        curl_close($ch);
                        if ($server_output == '1') return true;
                    } catch (\Exception $e) {

                    }
                } else if (isset(\Yii::$app->params['adHostname']) && ($adHostname = \Yii::$app->params['adHostname'])) {
//                    echo 'test';exit;
                    //try to authorize from exchange server
                    $ldap = ldap_connect($adHostname);
                    if ($bind = ldap_bind($ldap, $adUsername, $adPassword)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }
}
