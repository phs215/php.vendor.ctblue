<?php

namespace ctblue\web\auth\security\models;


use ctblue\web\auth\security\UserLoginUtil;
use Yii;

/**
 * This is the model class for table "user_permission".
 *
 * @property int $id
 * @property string $value
 * @property int $permission_id
 * @property int $user_id
 * @property string $modification_date
 * @property string $creation_date
 *
 * @property UserBaseModel $user
 * @property PermissionBaseModel $permission
 */
class UserPermissionBaseModel extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value', 'permission_id', 'user_id'], 'required'],
            [['permission_id', 'user_id'], 'integer'],
            [['modification_date', 'creation_date'], 'safe'],
            [['value'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['permission_id'], 'exist', 'skipOnError' => true, 'targetClass' => Permission::className(), 'targetAttribute' => ['permission_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'permission_id' => 'Permission ID',
            'user_id' => 'User ID',
            'modification_date' => 'Modification Date',
            'creation_date' => 'Creation Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getDb()
    {
        return \Yii::$app->dbUserManagement;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermission()
    {
        return $this->hasOne(PermissionBaseModel::className(), ['id' => 'permission_id']);
    }

    public static function checkByAd($permission, $adUsername)
    {
        if ($userRow = UserPermissionBaseModel::getDb()->createCommand("SELECT * FROM user WHERE ad_username=:username AND active=1", [':username' => $adUsername])->queryOne()) {
            $userId = $userRow['id'];
            return UserPermissionBaseModel::check($permission, $userId);
        }
        return false;
    }

    private static $permissionCache = [];

    public static function check($permission, $userId = false, $appKey = false)
    {
        $user = false;
        if (!$userId) {
            if ($user = UserLoginUtil::loggedInUser()) {
                //get the user id from the database
                if ($userUserDb = UserLoginUtil::findOneByUsername($user->ad_username)) {
                    $userId = $userUserDb->id;
                }
            }
        }
        if (!$userId) return false;
        $key = $permission . '-' . $userId;
        if (isset(UserPermissionBaseModel::$permissionCache[$key])) {
            return UserPermissionBaseModel::$permissionCache[$key];
        }
        if (!$appKey) {
            $appKey = \Yii::$app->params['app-id'];
        }
        if ($app = LinksBaseModel::findByStaticTitle($appKey)) {
            if ($permissionModel = PermissionBaseModel::findOne(['key' => $permission, 'application_id' => $app->id])) {
//                var_dump($permissionModel);
//                echo $userId;exit;
                if ($userPermission = UserPermissionBaseModel::findOne(['permission_id' => $permissionModel->id, 'user_id' => $userId])) {
                    if ($userPermission->value) {
                        UserPermissionBaseModel::$permissionCache[$key] = true;
                        return true;
                    }
                }
            }
        }
        UserPermissionBaseModel::$permissionCache[$key] = false;
        return false;
    }
}
