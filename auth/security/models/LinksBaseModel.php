<?php

namespace ctblue\web\auth\security\models;


use Yii;

/**
 * This is the model class for table "links".
 *
 * @property int $id
 * @property string $title
 * @property string $url
 * @property string|null $static_title
 * @property string $modification_date
 * @property string $creation_date
 *
 * @property PermissionBaseModel[] $permissions
 */
class LinksBaseModel extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->dbUserManagement;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['modification_date', 'creation_date'], 'safe'],
            [['title', 'static_title'], 'string', 'max' => 255],
            [['url'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'static_title' => 'Static Title',
            'modification_date' => 'Modification Date',
            'creation_date' => 'Creation Date',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermissions()
    {
        return $this->hasMany(PermissionBaseModel::className(), ['application_id' => 'id']);
    }

    public static $cacheByTitle = [];

    public static function findByStaticTitle($title)
    {
        if (isset(LinksBaseModel::$cacheByTitle[$title]) && ($app = LinksBaseModel::$cacheByTitle[$title])) return $app;
        if ($app = LinksBaseModel::findOne(['static_title' => $title])) {
            LinksBaseModel::$cacheByTitle[$title] = $app;
            return $app;
        }
        return false;
    }
}
