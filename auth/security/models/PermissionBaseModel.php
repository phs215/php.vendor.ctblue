<?php

namespace ctblue\web\auth\security\models;


use gpc\security\UserPermissionBaseModel;

/**
 * This is the model class for table "permission".
 *
 * @property int $id
 * @property string $key
 * @property int $application_id
 * @property string $modification_date
 * @property string $creation_date
 *
 * @property LinksBaseModel $application
 * @property UserPermissionBaseModel[] $userPermissions
 */
class PermissionBaseModel extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->dbUserManagement;
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'application_id'], 'required'],
            [['application_id'], 'integer'],
            [['modification_date', 'creation_date'], 'safe'],
            [['key'], 'string', 'max' => 255],
            [['application_id'], 'exist', 'skipOnError' => true, 'targetClass' => LinksBaseModel::className(), 'targetAttribute' => ['application_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'application_id' => 'Application ID',
            'modification_date' => 'Modification Date',
            'creation_date' => 'Creation Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplication()
    {
        return $this->hasOne(LinksBaseModel::className(), ['id' => 'application_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPermissions()
    {
        return $this->hasMany(UserPermissionBaseModel::className(), ['permission_id' => 'id']);
    }
}
