<?php
namespace ctblue\web\framework;

class Controller
{
    /**
     * includes the view file
     * @param $view
     * @param null $vars
     */
    public function render($view, $vars = null)
    {
        if ($vars) {
            extract($vars);
        }
        include('view/' . $view . '.php');
    }

    public static function getUrlRoute($controller, $controllerFunction)
    {
        return BASEURL . '/' . $controller . '/' . $controllerFunction . '/';
    }

    public static function routeTo($controller, $controllerFunction)
    {
        $controllerClass = ucfirst($controller) . 'Controller';
        include(BASEDIR . '/controllers/' . $controllerClass . '.php');
        $controllerClass = '\\controllers\\' . ucfirst($controller) . 'Controller';
        if ($controllerClass && $controllerFunction) {
            $class = new $controllerClass;
            if (method_exists($class, $controllerFunction)) {
                $class->{$controllerFunction}();
            }
        }
    }
}