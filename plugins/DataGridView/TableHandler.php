<?php
namespace ctblue\web\plugins\DataGridView;

use ctblue\web\Utils\Http;
use ctblue\web\Utils\DateTimeUtils;

class TableHandler
{
    private $table = '';
    private $cols = array();
    public $td = -1;

    function __construct($table)
    {
        //check if we are editing or adding or deleting
        $this->table = $table;
        $this->td = http::Get('td');
        //get the table's columns
//        $cmd = "SHOW COLUMNS FROM `" . $this->table . "`";
//        $this->cols = Database::returnFromDb($cmd);
    }

    public function Add()
    {
        //build add query
        //add
    }

    public function Edit()
    {
        //build edit query
        //add
    }

    public static function returnPostValue($post_name, $type)
    {
        //var_dump($_POST);
        $value = http::Post($post_name);
        $value = stripslashes($value);
        switch ($type) {
            case 'bool':
            case 'boolean':
                if ($value == 'disabled') {
                    $value = 1;
                }else if($value==1){
                    $value=1;
                }else if($value===true){
                    $value=1;
                }else if($value===false){
                    $value=0;
                }else{
                    $value=0;
                }
                break;
            case 'index':
                if ($value == "") {
                    return null;
                }
                break;
            case 'image':
            case
            'file':
//                $value = urldecode($value);
                break;
            case 'textarea':
                $value = nl2br($value);
                break;
            case 'date':
                $value = DateTimeUtils::FrenchToEnglish($value);
                break;
            case 'full_text':
            case 'fulltext':
                ///echo $value;
                break;
            case 'phone':
                $phone1 = http::Post('phone1');
                $phone2 = http::Post('phone2');
                $phone3 = http::Post('phone3');
                $value = $phone1 . '%cut%' . $phone2 . '%cut%' . $phone3;
                break;
            default:
                break;
        }
        return $value;
    }
} 