<?php
namespace ctblue\web\plugins\DataGridView;

use ctblue\web\Utils\Controls;
use ctblue\web\Utils\DateTimeUtils;
use ctblue\web\Utils\URL;

class DataGridFields
{
    private static $use_tiny_mce=false;
    public static function useTinyMCE(){
        self::$use_tiny_mce=true;
    }
    public static function GetData($result, $name)
    {
        if (isset($result[$name])) {
            return $result[$name];
        } else return '';
    }

    public static function GetEditField($result, $name, $type, $is_required = false, $is_read_only = false, $defaultValue = '', $params = array(), $new_name = '')
    {
        if($result){
            if(!is_array($result)){
                $result=get_object_vars($result);
            }
        }
        $type=str_replace('_','',$type);
        if($type=='fulltext' && self::$use_tiny_mce){
            $type='fulltext1';
        }
        $data = '';
        if (isset($result[$name])) $data = $result[$name];

        if ($data == '') {
            $data = $defaultValue;
        }

        global $baseurl;
        $tmp = "";
        $required = '';
        $disabled = '';
        if ($is_read_only) {
            $disabled = ' readonly ';
        }
        if ($is_required) {
            $required .= ' data-validation="required" data-validation-error-msg="Field is required" ';
            if ($type == 'password') {
                $required = ' data-validation="strength" data-validation-strength="2" ';
            }
        }
        if ($new_name != '') $name = $new_name;
        if (strpos($type, 'phone') !== false) {
            $tmp = explode('%cut%', $data);
            if (count($tmp) == 3) {
                if ($type == 'phone1') {
                    $data = $tmp[0];
                    $name .= '1';
                } else if ($type == 'phone2') {
                    $data = $tmp[1];
                    $name .= '2';
                } else {
                    $data = $tmp[2];
                    $name .= '3';
                }
            }
            $type = 'phone';
        }
        switch (strtolower($type)) {
            case 'int':
            case 'number':
            case 'font-size':
            case 'position':
                if ($data == '' && strtolower($type) == 'position') $data = 0;
                return '<input style="width:200px;" ' . $disabled . ' type="text" ' . $required . ' class="int-spinner form-control" autocomplete="off" name="' . $name . '" id="' . $name . '" value="' . $data . '"/>';
                break;
            case 'password':
                return '<input data-validation-optional="true" style="width:200px;" ' . $disabled . ' type="text" ' . $required . ' class="form-control" autocomplete="off" name="' . $name . '" id="' . $name . '" value=""/>';
                break;
            case 'text':
            case 'phone':
            case 'link':
            case 'youtube':
            case 'salt':
            case 'font-family':
                return '<input style="width:200px;" ' . $disabled . ' type="text" ' . $required . ' class="form-control" autocomplete="off" name="' . $name . '" id="' . $name . '" value="' . htmlspecialchars($data) . '"/>';
                break;
            case 'largetext':
            case 'textarea':
                return '<textarea cols="30" rows=10 ' . $disabled . ' class="form-control" ' . $required . ' name="' . $name . '" id="' . $name . '">' . $data . '</textarea>';
                break;
            case 'image':
            case 'picture':
//                return '<div><input style="width:200px;" '.$disabled.' '.$required.' type="text" class="form-control" autocomplete="off" name="' . $name . '" id="' . $name . '" value="' . $data . '"/> <button  class="btn btn-primary" onClick="javascript:browseServer(\'' . $name . '\');return false;">Browse server</button><button  class="btn btn-danger" onClick="$(this).parent().find(\'input:text\').val(\'\');return false;">Clear</button></div>';
                ob_start();
                ?>
                <script>
                    function open_popup(url) {
                        var w = 880;
                        var h = 570;
                        var l = Math.floor((screen.width - w) / 2);
                        var t = Math.floor((screen.height - h) / 2);
                        var win = window.open(url, 'File manager', "scrollbars=1,width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
                    }
                </script>
                <div><input style="width:200px;" <?php echo $disabled . ' ' . $required ?> type="text"
                            class="form-control" autocomplete="off" name="<?php echo $name ?>" id="<?php echo $name ?>"
                            value="<?php echo $data ?>"/>
                    <button class="btn btn-primary"
                            onClick="javascript:open_popup('<?php echo $baseurl ?>/plugins/filemanager/dialog.php?type=1&popup=1&field_id=<?php echo $name ?>');return false;">
                        Choose an image
                    </button>
                    <button class="btn btn-danger" onClick="$(this).parent().find('input:text').val('');return false;">
                        Clear
                    </button>
                </div>
                <?php
                return ob_get_clean();
                break;
            case 'file':
            case 'pdf':
                ob_start();
                ?>
                <script>
                    function open_popup(url) {
                        var w = 880;
                        var h = 570;
                        var l = Math.floor((screen.width - w) / 2);
                        var t = Math.floor((screen.height - h) / 2);
                        var win = window.open(url, 'File manager', "scrollbars=1,width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
                    }
                </script>
                <div><input style="width:200px;" <?php echo $disabled . ' ' . $required ?> type="text"
                            class="form-control" autocomplete="off" name="<?php echo $name ?>" id="<?php echo $name ?>"
                            value="<?php echo $data ?>"/>
                    <button class="btn btn-primary"
                            onClick="javascript:open_popup('<?php echo $baseurl ?>/plugins/filemanager/dialog.php?type=2&popup=1&field_id=<?php echo $name ?>');return false;">
                        Choose file
                    </button>
                    <button class="btn btn-danger" onClick="$(this).parent().find('input:text').val('');return false;">
                        Clear
                    </button>
                </div>
                <?php
                return ob_get_clean();
                break;
            case 'date':
                if ($data != 'now') {
                    $data = DateTimeUtils::EnglishToFrench($data);
                }
                return Controls::DateJqueryUI($name, false, $data, "", ($required) ? true : false);
                break;
            case 'date-now':
            case 'date-today':
                $data = DateTimeUtils::EnglishToFrench(date('dd-mm-YYYY'));
                return Controls::DateJqueryUI($name, false, $data, "", ($required) ? true : false);
                break;
            case 'time':
                if ($data == 'now') {
                    return Controls::TimeJqueryUI($name, $data, "var d=new Date();obj.val(d.getHours()+':'+((d.getMinutes()<10)?'0':'')+d.getMinutes())", ($required) ? true : false);
                }
                return Controls::TimeJqueryUI($name, $data, '', ($required) ? true : false);
                break;
            case 'time-now':
                return Controls::TimeJqueryUI($name, $data, "var d=new Date();obj.val(d.getHours()+':'+d.getMinutes())", ($required) ? true : false);
                break;
            case 'country':
                return Controls::printCountries($db,$name, $data);
                break;
            case 'fulltext':

                $tmp .= '<textarea ' . $disabled . ' ' . $required . ' id="' . $name . '" name="' . $name . '" >' . $data .
                    '</textarea>';
                ob_start();
                ?>
                <script type="text/javascript" src="<?php echo $baseurl?>/plugins/wysiwyg1/ckeditor/ckeditor.js"></script>
                <script>
                    CKEDITOR.replace( "<?php echo $name?>" ,
                        {
                            filebrowserBrowseUrl : "<?php echo $baseurl?>/plugins/wysiwyg1/filemanager/browser/default/browser.html?Connector=<?php echo urlencode($baseurl)?>%2Fplugins%2Fwysiwyg1%2Ffilemanager%2Fconnectors%2Fphp%2Fconnector.php",
                            filebrowserImageBrowseUrl : "<?php echo $baseurl?>/plugins/wysiwyg1/filemanager/browser/default/browser.html?Connector=<?php echo urlencode($baseurl)?>%2Fplugins%2Fwysiwyg1%2Ffilemanager%2Fconnectors%2Fphp%2Fconnector.php",
                            filebrowserFlashBrowseUrl : "<?php echo $baseurl?>/plugins/wysiwyg1/filemanager/browser/default/browser.html?Connector=<?php echo urlencode($baseurl)?>%2Fplugins%2Fwysiwyg1%2Ffilemanager%2Fconnectors%2Fphp%2Fconnector.php",
                            filebrowserUploadUrl : "<?php echo $baseurl?>%2Fplugins/wysiwyg1/filemanager/browser/default/browser.html?Connector=<?php echo urlencode($baseurl)?>%2plugins%2FFwysiwyg1%2Ffilemanager%2Fconnectors%2Fphp%2Fconnector.php",
                            filebrowserImageUploadUrl : "<?php echo $baseurl?>/plugins/wysiwyg1/filemanager/browser/default/browser.html?Connector=<?php echo urlencode($baseurl)?>%2Fplugins%2Fwysiwyg1%2Ffilemanager%2Fconnectors%2Fphp%2Fconnector.php",
                            filebrowserFlashUploadUrl : "<?php echo $baseurl?>/plugins/wysiwyg1/filemanager/browser/default/browser.html?Connector=<?php echo urlencode($baseurl)?>%2Fplugins%2Fwysiwyg1%2Ffilemanager%2Fconnectors%2Fphp%2Fconnector.php",
                            coreStyles_bold : { element :'b', overrides : 'strong' }
                        });
                </script>
                <?php
                $tmp.=ob_get_clean();
                break;
            case "full_text1":
            case 'fulltext1':
                $tmp .= '<textarea ' . $disabled . ' ' . $required . ' id="' . $name . '" name="' . $name . '" >' . $data .
                    '</textarea>';
                $width = '100%';
                $height = 300;
                $width = (isset($params['width'])) ? $params['width'] : $width;
                if ($width[strlen($width) - 1] == '%') $width = "'" . $width . "'";
                $height = (isset($params['height'])) ? $params['height'] : $height;
                if ($height[strlen($height) - 1] == '%') $height = "'" . $height . "'";
                ob_start();
                ?>
                <script type="text/javascript">
                    $(document).ready(function () {
                        tinymce.init({
                            selector: "#<?php echo $name?>",
                            extended_valid_elements: "i/em",
                            width: <?php echo $width?>,
                            height: <?php echo $height?>,
                            resize: "both",
                            theme: "modern",
                            fontsize_formats: "8pt 9pt 10pt 11pt 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
                            plugins: [
                                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                "searchreplace wordcount visualblocks visualchars code fullscreen",
                                "insertdatetime media nonbreaking save table contextmenu directionality",
                                "emoticons template paste textcolor youtube"
                            ],
                            theme_advanced_buttons3_add: "fullscreen",
                            force_p_newlines: false,
                            forced_root_block: '', // Needed for 3.x
                            toolbar1: "fullscreen | undo redo  | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                            toolbar2: "| responsivefilemanager | print preview media | forecolor backcolor emoticons youtube | fontselect | fontsizeselect | ltr rtl",
                            image_advtab: true,
                            external_filemanager_path: "<?php echo $baseurl?>/plugins/filemanager/",
                            filemanager_title: "Responsive Filemanager",
                            external_plugins: {"filemanager": "<?php echo $baseurl?>/plugins/filemanager/plugin.min.js"},
                            relative_urls: false,
                            remove_script_host: false,
                            style_formats: [
                                {
                                    title: "Interlignage", items: [
                                    <?php
                                    for($i=2;$i<100;$i+=2){
                                        echo '{title: "'.$i.'", inline: \'div\', styles: {\'line-height\': \''.$i.'px\'}},';
                                    }
                                    ?>
                                ]
                                },
                                {
                                    title: 'Headers', items: [
                                    {title: 'Header 1', block: 'h1'},
                                    {title: 'Header 2', block: 'h2'},
                                    {title: 'Header 3', block: 'h3'},
                                    {title: 'Header 4', block: 'h4'},
                                    {title: 'Header 5', block: 'h5'},
                                    {title: 'Header 6', block: 'h6'}
                                ]
                                },
                                {
                                    title: 'Inline', items: [
                                    {title: 'Bold', icon: "bold", inline: 'strong'},
                                    {title: 'Italic', icon: "italic", inline: 'em'},
                                    {
                                        title: 'Underline',
                                        icon: "underline",
                                        inline: 'span',
                                        styles: {'text-decoration': 'underline'}
                                    },
                                    {
                                        title: 'Strikethrough',
                                        icon: "strikethrough",
                                        inline: 'span',
                                        styles: {'text-decoration': 'line-through'}
                                    },
                                    {title: 'Superscript', icon: "superscript", inline: 'sup'},
                                    {title: 'Subscript', icon: "subscript", inline: 'sub'},
                                    {title: 'Code', icon: "code", inline: 'code'}
                                ]
                                },
                                {
                                    title: 'Blocks', items: [
                                    {title: 'Paragraph', block: 'p'},
                                    {title: 'Blockquote', block: 'blockquote'},
                                    {title: 'Div', block: 'div'},
                                    {title: 'Pre', block: 'pre'}
                                ]
                                },
                                {
                                    title: 'Alignment', items: [
                                    {title: 'Left', icon: "alignleft", block: 'div', styles: {'text-align': 'left'}},
                                    {
                                        title: 'Center',
                                        icon: "aligncenter",
                                        block: 'div',
                                        styles: {'text-align': 'center'}
                                    },
                                    {title: 'Right', icon: "alignright", block: 'div', styles: {'text-align': 'right'}},
                                    {
                                        title: 'Justify',
                                        icon: "alignjustify",
                                        block: 'div',
                                        styles: {'text-align': 'justify'}
                                    }
                                ]
                                }
                            ]
                        });
                    });</script>
                <?php
                $tmp .= ob_get_clean();
                break;
            case 'bool':
                $tmp = '<input type="checkbox" ' . $disabled . ' name="' . $name . '" id="' . $name . '" value="disabled" ';
                if ($data) {
                    $tmp .= 'checked="checked"';
                }
                $tmp .= '/>';
                break;
            case 'color':
                return Controls::ColorPicker($name, $data);
                break;
        }
        return $tmp;
    }

    public static function GetField($data, $type)
    {
        switch ($type) {
            case 'font-family':
                return '<span style="font-family:' . $data . '">The Brown Fox Jumps over the Lazy Dog (' . $data . ')</span>';
                break;
            case 'font-size':
                return '<span style="font-size:' . $data . '">The Brown Fox Jumps over the Lazy Dog (' . $data . 'px)</span>';
                break;
            case 'image':
            case 'picture':
                if ($data != "") {
                    return '<img src="' . $data . '" style="max-width:200px;max-height:200px;"/>';
                }
                break;
            case 'date':
                $data = DateTimeUtils::EnglishToFrench($data);
                break;
            case 'link':
                return '<a href="' . $data . '" target="_blank">' . $data . '</a>';
            case 'bool':
                if ($data == 1) {
                    return 'Yes';
                } else
                    return 'No';
                break;
            case 'password':
                return 'Hidden';
            case 'color':
                return '<div style="width:100%;height:100%;background-color:' . $data . '">&nbsp;</div>';
            case 'youtube':
                return '<iframe class="youtube-player" type="text/html" width="100" height="100" src="http://www.youtube.com/embed/' . $data . '" frameborder="0"></iframe>';
            case 'fulltext':
                return '<div class="plain_text m_over" onclick="javascript:showTextParent($(this).html())">' . $data . '</div>';
            default:
                return '<div class="plain_text">' . $data . '</div>';
                break;
        }
        return $data;
    }

    public static function GetSaveButton()
    {
        return '<input type="submit" name="save" class="btn btn-success"
                   value="Save"/>';
    }

    public static function GetBackButton($url)
    {
        return '<input type="submit" name="back" onclick="window.location=\'' . $url . '\'" class="btn btn-info"
                   value="Back"/>';
    }

    public static function GetNewButton()
    {
        global $currenturl;
        return '<input type="submit" name="new" onclick="window.location=\'' . URL::AddParams(URL::RemoveParam($currenturl, 'td', true), 'td=new-1') . '\'" class="btn btn-success"
                   value="Add New"/>';
    }

    public static function GetEditDeleteButtons($id, $td_edit = 'edit', $td_delete = 'delete')
    {
        $url=CURRENT_URL;
        ?>
        <div style="float:right"><span style="margin-left:10px;"><button class="btn btn-primary"
                                                                         onclick="window.location='<?php echo $url ?>?td=<?php echo $td_edit ?>&id=<?php echo $id ?>'">
                    Edit
                </button>
                <?php if ($td_delete !== false) {
                    ?>
                    <button class="btn btn-danger" style="margin-left:10px;"
                            onclick="window.location='<?php echo $url ?>?td=<?php echo $td_delete ?>&id=<?php echo $id ?>'">
                        Delete
                    </button>
                    <?php
                }
                ?>
                </span>
        </div>
        <?php
    }
} 