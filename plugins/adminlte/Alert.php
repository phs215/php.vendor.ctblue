<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 2/10/2020
 * Time: 11:46 AM
 */

namespace ctblue\web\plugins\adminlte;


class Alert
{
    public static function display($title, $message, $type = 'info', $icon = 'fas fa-ban')
    {
        ob_start();
        ?>
        <div class="alert alert-<?= $type ?> alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="<?= $icon ?>"></i><?= $title ?></h4>
            <?= $message ?>
        </div>
        <?php
        return ob_get_clean();
    }
}