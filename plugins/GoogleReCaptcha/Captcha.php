<?php
namespace ctblue\web\plugins\GoogleReCaptcha;

class Captcha {
    private static $enabled=false;
    private static $private_key='';
    private static $public_key='';
    public static function Init($private_key,$public_key){
        self::$private_key=$private_key;
        self::$public_key=$public_key;
    }
    public static function Verify(){
        if(!self::$enabled)return true;
        if (!isset($_POST['g-recaptcha-response'])) {
            return false;
        }
//        return true;
        $captcha='';
        if(isset($_POST['g-recaptcha-response']))
            $captcha=$_POST['g-recaptcha-response'];

//        if(!$captcha){
//            echo '<h2>Please check the the captcha form.</h2>';
//            exit;
//        }
        $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".self::$private_key."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        if($response['success'] == false)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public static function IncludeCaptcha(){
        if(!self::$enabled)return;
        require_once(dirname(__FILE__) . '/recaptchalib.php');
        ?>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <div class="g-recaptcha" data-sitekey="<?php echo self::$public_key?>"></div>
        <?php
    }
} 