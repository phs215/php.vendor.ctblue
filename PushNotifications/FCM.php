<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 8/29/2018
 * Time: 6:58 PM
 */

namespace ctblue\web\PushNotifications;


class FCM
{
    public $authorizationKey = '';
    public $isError=false;

    function __construct($authorizationKey)
    {
        $this->authorizationKey = $authorizationKey;
    }

    public function send($subject,$body, $registrationId)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POSTFIELDS => '{
 "to" : "' . $registrationId . '",
 "collapse_key" : "type_a",
 "notification" : {
     "body" : "' . $body . '",
     "title": "' . $subject . '",
     "sound":"default"
 },
 "headers": {
        "Urgency": "high",
        "sound":1
      },
}',
            CURLOPT_HTTPHEADER => array(
                "Authorization: key=" . $this->authorizationKey,
                "Cache-Control: no-cache",
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $this->isError=true;
            return $err;
        } else {
            return $response;
        }
    }
}