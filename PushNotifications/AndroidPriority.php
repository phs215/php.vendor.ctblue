<?php
/**
 * Created by PhpStorm.
 * User: phili
 * Date: 3/27/2016
 * Time: 5:04 PM
 */

namespace ctblue\web\Utils\PushNotifications;


class AndroidPriority
{
    public static $high='high';
    public static $normal='normal';
    public static $low='low';
}