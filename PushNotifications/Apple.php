<?php
/**
 * Created by PhpStorm.
 * User: phili
 * Date: 3/28/2016
 * Time: 3:54 PM
 */

namespace ctblue\web\PushNotifications;


class Apple
{
    private $deviceTokens = array();
    private $passPhrase = '';
    private $apns_file_pem = 'apns-dev.pem';

    public function setAPNSPemFile($file)
    {
        $this->apns_file_pem = $file;
    }

    public function setPassphrase($phrase)
    {
        $this->passPhrase = $phrase;
    }

    public function addDevice($deviceToken)
    {
        $this->deviceTokens[] = $deviceToken;
    }

    public function addDeviceArray($deviceTokens)
    {
        $this->deviceTokens = array_merge($deviceTokens, $this->deviceTokens);
    }

    public function Push($message)
    {
        $ctx = stream_context_create();
//        file_put_contents('log',$this->apns_file_pem."\r\n",FILE_APPEND);
//        file_put_contents('log',$this->passPhrase."\r\n",FILE_APPEND);
        stream_context_set_option($ctx, 'ssl', 'local_cert', $this->apns_file_pem);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $this->passPhrase);

// Open a connection to the APNS server
        $fp = stream_socket_client(
            'ssl://gateway.sandbox.push.apple.com:2195', $err,
            $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'badge' => 0
        );
// Encode the payload as JSON
        $payload = json_encode($body);

        $c = count($this->deviceTokens);
// Build the binary notification
        for ($i = 0; $i < $c; $i++) {
//            echo $this->deviceTokens[$i];
            $msg = chr(0) . pack('n', 32) . pack('H*', $this->deviceTokens[$i]) . pack('n', strlen($payload)) . $payload;
            // Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));
            //var_dump($result);
            if (!$result)
                echo 'Message not delivered' . PHP_EOL;
            else
                echo 'Message successfully delivered' . PHP_EOL;
        }

// Close the connection to the server
        fclose($fp);
    }
}